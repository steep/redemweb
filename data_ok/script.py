def get_list_country_from_csv(file, pos):
  with open(file, 'r') as fd:
    content = fd.read()
  
  out = []
  lines = content.split('\n')
  for line in lines:
    csv_word_list = line.split(';')
    out.append(csv_word_list[pos])
  
  return out

############
def remove_underscore(name):
  return ' '.join(name.split('_'))


def get_list_name(lines, nb):
  out = []
  for line in lines:
    line = line.split(';')
    if line[1] == nb:
      out.append(remove_underscore(line[2]))
  
  return out

def put_country_id():
  with open('../list_of_country_codes.csv', 'r') as fd:
    list_name_code = fd.read().split('\n')
  list_name_code = list_name_code[1:]
  
  with open('../emissions_LUC_Julia.csv', 'r') as fd:
    list_LULUCF = fd.read().split('\n')
  
  list_LULUCF = list_LULUCF[1:]
  out = ''
  
  for line_LULUCF in list_LULUCF:
    line_LULUCF = line_LULUCF.split(';')
    print(line_LULUCF[0])
    list_name = get_list_name(list_name_code, line_LULUCF[0])
    if len(list_name) > 1:
      print(list_name)
    out = out + ';'.join(list_name) + ';' + ';'.join(line_LULUCF[1:]) + '\n'
  
  with open('LULUCF_TMP.csv', 'w') as fd:
    fd.write(out)


def adapt_unit():
  with open('LULUCF_TMP.csv', 'r') as fd:
    list_lulucf = fd.read().split('\n')
  
  out = ''
  
  for line in list_lulucf:
    line = line.split(';')
    out = out + line[0]
    print(line)
    for elem in line[1:]:
      try:
        new_value = float(elem)
        new_value = new_value / 10**9
        out = out + ';' + str(new_value)
      except ValueError:
        out = out + ';' + elem
    out = out + '\n'
  
  with open('NEW_LULUCF_TMP.csv', 'w') as fd:
    fd.write(out)

def desagrege_data():
  with open('vrai_lulucf.csv', 'r') as fd:
    list_lulucf = fd.read().split('\n')
  
  out = ''
  
  nb = 0
  
  premier_line = list_lulucf[0].split(';')
  for elem in premier_line:
    try:
      float(elem)
      break
    except ValueError:
      nb = nb + 1
  
  new_values = []
  
  for i in range(nb, len(premier_line)):
    new_elem = float(premier_line[i])
    new_elem = new_elem / nb
    new_values.append(new_elem)
  
  for i in range(nb):
    out = out + premier_line[i]
    for elem in new_values:
      out = out + ';' + str(elem)
    
    out = out + '\n'
  
  with open('tmp.csv', 'w') as fd:
    fd.write(out)
  
############

def create_association_from_file(file):
  with open(file, 'r') as fd:
    list_assos = fd.read().split('\n')
  
  out = {}
  
  for assos in list_assos:
    assos = assos.split(';')
    out[assos[0]] = assos[1]
  
  return out


def inv_dict(dic):
  return {v: k for k, v in dic.items()}


def replace_id_with_dic(file, dic):
  with open(file, 'r') as fd:
    content = fd.read().split('\n')
  
  out = content[0] + '\n'
  
  content = content[1:]
  
  for line in content:
    line = line.split(';')
    out = out + dic[line[0]] + ';' + ';'.join(line[1:]) + '\n'
  
  return out


if __name__ == '__main__':
  dic = create_association_from_file('country_name_english.csv')
  new_content = replace_id_with_dic('population.csv', dic)
  print(dic)
  print('\n\n\n')
  print(new_content)
  print('\n\n\n')
  
  with open('new_population.csv', 'w') as fd:
    fd.write(new_content)
    
  










































