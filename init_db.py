import sqlite3

conn = sqlite3.connect('climate_data.db')
cursor = conn.cursor()

#### TABLE DataSet
cursor.execute("""CREATE TABLE DataSet (
                    id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                    name VARCHAR(100),
                    author VARCHAR(100),
                    comment VARCHAR(100),
                    id_country INTEGER,
                    id_region INTEGER,
                    id_emission_co2 INTEGER,
                    id_emission_ghg INTEGER,
                    id_LULUCF INTEGER,
                    id_gdp INTEGER,
                    id_population INTEGER,
                    id_INDC INTEGER,
                    id_budget INTEGER,
                    units_emission VARCHAR(100),
                    units_gdp VARCHAR(100),
                    units_population VARCHAR(100),
                    FOREIGN KEY (id_country) REFERENCES Country(id),
                    FOREIGN KEY (id_region) REFERENCES Region(id),
                    FOREIGN KEY (id_emission_co2) REFERENCES Emission_CO2(id),
                    FOREIGN KEY (id_emission_ghg) REFERENCES Emission_Other_GHG(id),
                    FOREIGN KEY (id_LULUCF) REFERENCES LULUCF(id),
                    FOREIGN KEY (id_gdp) REFERENCES GDP(id),
                    FOREIGN KEY (id_population) REFERENCES Population(id),
                    FOREIGN KEY (id_INDC) REFERENCES INDC(id),
                    FOREIGN KEY (id_budget) REFERENCES Budget(id)
                  )
""")
conn.commit()

#### TABLE Budget
cursor.execute("""CREATE TABLE Budget (
                    id INTEGER,
                    temperature FLOAT,
                    main_budget FLOAT,
                    year_ini INTEGER,
                    year_final INTEGER,
                    PRIMARY KEY(id, temperature)
                  )
""")
conn.commit()

#### TABLE Country
cursor.execute("""CREATE TABLE Country (
                    id INTEGER,
                    id_country INTEGER,
                    PRIMARY KEY(id, id_country)
                  )
""")
conn.commit()

#### TABLE Name_country
cursor.execute("""CREATE TABLE Name_country (
                    id_country_table INTEGER,
                    id_country INTEGER,
                    language VARCHAR(100),
                    name VARCHAR(100),
                    FOREIGN KEY (id_country_table, id_country) REFERENCES Country(id, id_country)
                  )
""")
conn.commit()


#### TABLE Region
cursor.execute("""CREATE TABLE Region (
                    id INTEGER,
                    id_region INTEGER,
                    id_country INTEGER,
                    FOREIGN KEY (id_region) REFERENCES Country(id_country),
                    FOREIGN KEY (id_country) REFERENCES Country(id_country)
                  )
""")
conn.commit()

#### TABLE GDP
cursor.execute("""CREATE TABLE GDP (
                    id INTEGER,
                    id_country INTEGER,
                    year INTEGER,
                    gdp FLOAT,
                    FOREIGN KEY (id_country) REFERENCES Country(id_country)
                  )
""")
conn.commit()

#### TABLE Emission_CO2
cursor.execute("""CREATE TABLE Emission_CO2 (
                    id INTEGER,
                    id_country INTEGER,
                    year INTEGER,
                    emission FLOAT,
                    FOREIGN KEY (id_country) REFERENCES Country(id_country)
                  )
""")
conn.commit()

#### TABLE Emission_Other_GHG
cursor.execute("""CREATE TABLE Emission_Other_GHG (
                    id INTEGER,
                    temperature FLOAT,
                    id_country INTEGER,
                    year INTEGER,
                    emission FLOAT,
                    FOREIGN KEY (id_country) REFERENCES Country(id_country)
                  )
""")
conn.commit()

#### TABLE LULUCF
cursor.execute("""CREATE TABLE LULUCF (
                    id INTEGER,
                    id_country INTEGER,
                    year INTEGER,
                    emission FLOAT,
                    FOREIGN KEY (id_country) REFERENCES Country(id_country)
                  )
""")
conn.commit()

#### TABLE Population
cursor.execute("""CREATE TABLE Population (
                    id INTEGER,
                    id_country INTEGER,
                    year INTEGER,
                    population INTEGER,
                    FOREIGN KEY (id_country) REFERENCES Country(id_country)
                  )
""")
conn.commit()

#### TABLE INDC
cursor.execute("""CREATE TABLE INDC (
                    id INTEGER,
                    id_area INTEGER,
                    year INTEGER,
                    indc_min FLOAT,
                    indc_max FLOAT
                  )
""")
conn.commit()

#### TABLE CURVE
cursor.execute("""CREATE TABLE CURVE (
                    id INTEGER,
                    x FLOAT,
                    y FLOAT
                  )
""")
conn.commit()

#### TABLE PARAMETERS
cursor.execute("""CREATE TABLE PARAMETERS (
                    id_data INTEGER,
                    free_mu FLOAT,
                    temperature FLOAT,
                    year_max FLOAT,
                    r FLOAT,
                    gamma FLOAT,
                    theta FLOAT,
                    free_sigma FLOAT
                  )
""")
conn.commit()
