import numpy as np
import sqlite3
from scipy.interpolate import interp1d


import matplotlib.pyplot as plt



def list_str_to_list_int(l):
  out = []
  for elem in l:
    out.append(int(elem))
  
  return out


def homogenize_data(x, y):
  xmin = min(x)
  xmax = max(x)
  
  f = interp1d(x, y, kind='cubic')
  new_x = list(range(xmin, xmax + 1))
  new_y = []
  
  for elem in new_x:
    new_y.append(float(f(elem)))
  
  return (new_x, new_y)


def read_tab_from_csv(file):
  with open(file, 'r') as fd:
    lines = fd.read().split('\n')
  
  new_x = list_str_to_list_int((lines[0].split(';'))[1:])
  
  out = {}
  
  for line in lines[1:]:
    line = line.split(';')
    label = line[0]
    new_y = []
    
    for y in line[1:]:
      new_y.append(y)
    
    out[label] = new_y
  
  return (new_x, out)


def read_from_csv(file, separator=';'):
  with open(file, 'r') as fd:
    lines = fd.read().split('\n')
  
  out = []
  for line in lines:
    out.append(line.split(separator))
  
  return out


def insert_country_from_csv(conn, cursor, file, id):
  csv_content = read_from_csv(file)
  for country_info in csv_content:
    data = {"id" : id, "id_country" : country_info[1]}
    cursor.execute("""INSERT INTO Country(id, id_country) VALUES(:id, :id_country)""", data)
  
  conn.commit()


def insert_country_name_from_csv(conn, cursor, file, id, language):
  csv_content = read_from_csv(file)
  
  for country_info in csv_content:
    data = {'id_country_table' : id, 'id_country' : country_info[1], 'language' : language, 'name' : country_info[0]}
    cursor.execute("""INSERT INTO Name_country(id_country_table, id_country, language, name)
                      VALUES(:id_country_table, :id_country, :language, :name)""", data)
  
  conn.commit()


def insert_region_from_csv(conn, cursor, file, id):
  csv_content = read_from_csv(file)
  for region_info in csv_content:
    id_region = region_info[0]
    
    for id_country in region_info[1:]:
      data = {'id' : id, 'id_region' : id_region, 'id_country' : id_country}
      cursor.execute("""INSERT INTO Region(id, id_region, id_country)
                        VALUES(:id, :id_region, :id_country)""", data)
  
  conn.commit()


def insert_budget_from_csv(conn, cursor, file, id):
  csv_content = read_from_csv(file)
  
  for budget_info in csv_content:
    data = {
      'id' : id, 'temperature' : budget_info[0],
      'main_budget' : budget_info[1], 'year_ini' : budget_info[2],
      'year_final' : budget_info[3]
    }
    cursor.execute("""INSERT INTO Budget(id, temperature, main_budget, year_ini, year_final)
                      VALUES(:id, :temperature, :main_budget, :year_ini, :year_final)""", data)
  
  conn.commit()


def insert_emissions_co2_from_csv(conn, cursor, file, id):
  (emission_x, y_list) = read_tab_from_csv(file)
  
  for country, country_emission_y in y_list.items():
    (country_emission_x, country_emission_y) = homogenize_data(emission_x, country_emission_y)
    
    for year, emission in zip(country_emission_x, country_emission_y):
      data = {'id' : id, 'id_country' : country, 'year' : year, 'emission' : emission}
      cursor.execute("""INSERT INTO Emission_CO2(id, id_country, year, emission)
                        VALUES(:id, :id_country, :year, :emission)""", data)


def insert_emissions_ghg_from_csv(conn, cursor, file, id, temperature):
  (emission_x, y_list) = read_tab_from_csv(file)
    
  for country, country_emission_y in y_list.items():
    (country_emission_x, country_emission_y) = homogenize_data(emission_x, country_emission_y)
      
    for year, emission in zip(country_emission_x, country_emission_y):
      data = {'id' : id, 'temperature' : temperature, 'id_country' : country, 'year' : year, 'emission' : emission}
      cursor.execute("""INSERT INTO Emission_Other_GHG(id, temperature, id_country, year, emission)
                        VALUES(:id, :temperature, :id_country, :year, :emission)""", data)


def insert_emissions_lulucf_from_csv(conn, cursor, file, id):
  (emission_x, y_list) = read_tab_from_csv(file)
  
  for country, country_emission_y in y_list.items():
    (country_emission_x, country_emission_y) = homogenize_data(emission_x, country_emission_y)
    
    for year, emission in zip(country_emission_x, country_emission_y):
      data = {'id' : id, 'id_country' : country, 'year' : year, 'emission' : emission}
      cursor.execute("""INSERT INTO LULUCF(id, id_country, year, emission)
                        VALUES(:id, :id_country, :year, :emission)""", data)


def insert_population_from_csv(conn, cursor, file, id):
  (country_population_x, y_list) = read_tab_from_csv(file)
  
  for country, country_population_y in y_list.items():
    for year, population in zip(country_population_x, country_population_y):
      if population != '':
        data = {'id' : id, 'id_country' : country, 'year' : year, 'population' : float(population)}
        cursor.execute("""INSERT INTO Population(id, id_country, year, population)
                        VALUES(:id, :id_country, :year, :population)""", data)
  
  conn.commit()


def insert_gdp_from_csv(conn, cursor, file, id):
  (country_gdp_x, y_list) = read_tab_from_csv(file)
  
  for country, country_gdp_y in y_list.items():
    for year, gdp in zip(country_gdp_x, country_gdp_y):
      if gdp != '':
        data = {'id' : id, 'id_country' : country, 'year' : year, 'gdp' : float(gdp)}
        cursor.execute("""INSERT INTO GDP(id, id_country, year, gdp)
                        VALUES(:id, :id_country, :year, :gdp)""", data)

  conn.commit()


def insert_indc_from_csv(conn, cursor, file, id):
  csv_content = read_from_csv(file)
  
  for indc_info in csv_content[1:]:
    data = {'id' : id, 'id_area' : indc_info[0], 'year' : indc_info[1], 'indc_min' : indc_info[2], 'indc_max' : indc_info[3]}
    cursor.execute("""INSERT INTO INDC(id, id_area, year, indc_min, indc_max)
                      VALUES(:id, :id_area, :year, :indc_min, :indc_max)""", data)
  
  conn.commit()


def insert_dataset(conn, cursor, name, author, comment,
                   id_country, id_region, id_emission_co2, id_emission_ghg,
                   id_LULUCF, id_gdp, id_population, id_INDC, id_budget,
                   units_emission, units_gdp, units_population):
  data = {
    'name' : name,
    'author' : author,
    'comment' : comment,
    'id_country' : id_country,
    'id_region' : id_region,
    'id_emission_co2' : id_emission_co2,
    'id_emission_ghg' : id_emission_ghg,
    'id_LULUCF' : id_LULUCF,
    'id_gdp' : id_gdp,
    'id_population' : id_population,
    'id_INDC' : id_INDC,
    'id_budget' : id_budget,
    'units_emission' : units_emission,
    'units_gdp' : units_gdp,
    'units_population' : units_population
  }
  
  cursor.execute("""
  INSERT INTO DataSet(name, author, comment, id_country, 
                      id_region, id_emission_co2, id_emission_ghg,
                      id_LULUCF, id_gdp, id_population, id_INDC, 
                      id_budget, units_emission, units_gdp, units_population)
  VALUES(:name, :author, :comment, :id_country,
         :id_region, :id_emission_co2, :id_emission_ghg,
         :id_LULUCF, :id_gdp, :id_population, :id_INDC,
         :id_budget, :units_emission, :units_gdp, :units_population)""", data)
  
  conn.commit()  


def insert_data(name_data, author_data, comment_data,
                country_file, country_name_file_list, id_db_country,
                region_file, id_db_region,
                budget_file, id_db_budget, emission_co2_file,
                id_db_emission_co2, emission_ghg_file_dic, id_db_emission_ghg,
                lulucf_file, id_db_lulucf, gdp_file, id_db_gdp,
                population_file, id_db_population, indc_file, id_db_indc,
                units_emission_db, units_gdp_db, units_population_db):

  conn = sqlite3.connect('climate_data.db')
  cursor = conn.cursor()
  
  #### Country code insertion
  insert_country_from_csv(conn, cursor, country_file, id_db_country)
  
  #### Country Full name insertion in several languages
  for language, country_name_file in country_name_file_list.items():
    insert_country_name_from_csv(conn, cursor, country_name_file, id_db_country, language)
  
  #### Region code insertion
  insert_region_from_csv(conn, cursor, region_file, id_db_region)
  
  #### Budget insertion
  insert_budget_from_csv(conn, cursor, budget_file, id_db_budget)
  
  #### CO2 Emissions insertion
  insert_emissions_co2_from_csv(conn, cursor, emission_co2_file, id_db_emission_co2)
  
  #### Other GHG Emissions insertion
  for temperature, ghg_file in emission_ghg_file_dic.items():
    insert_emissions_ghg_from_csv(conn, cursor, ghg_file, id_db_emission_ghg, temperature)
  
  #### LULUCF insertion
  insert_emissions_lulucf_from_csv(conn, cursor, lulucf_file, id_db_lulucf)
  
  #### Population insertion
  insert_population_from_csv(conn, cursor, population_file, id_db_population)
  
  #### GDP insertion
  insert_gdp_from_csv(conn, cursor, gdp_file, id_db_gdp)
  
  #### INDC insertion
  insert_indc_from_csv(conn, cursor, indc_file, id_db_indc)
  
  #### Dataset insertion
  insert_dataset(conn, cursor, name_data, author_data, comment_data,
                 id_db_country, id_db_region, id_db_emission_co2, id_db_emission_ghg,
                 id_db_lulucf, id_db_gdp, id_db_population, id_db_indc, id_db_budget,
                 units_emission_db, units_gdp_db, units_population_db)
  
  conn.close()



if __name__ == '__main__':
  insert_data('EDGAR', '', '', 'data_ok/country_name_english.csv',
              {
                'English' : 'data_ok/country_name_english.csv',
                'French' : 'data_ok/country_name_french.csv'
              }, 1,
              'data_ok/new_region.csv', 1,
              'data_ok/new_budget.csv', 1, 'data_ok/new_emissions.csv',
              1, {
                1.5 : 'data_ok/new_ghg.csv',
                2.0 : 'data_ok/new_ghg.csv',
                2.5 : 'data_ok/new_ghg.csv',
                3.0 : 'data_ok/new_ghg.csv',
              }, 1,
              'data_ok/new_lulucf.csv', 1, 'data_ok/new_gdp.csv', 1,
              'data_ok/new_population.csv', 1, 'data_ok/new_indc.csv', 1,
              'kton CO2(eq)', 'US$', 'people')
  
  
  