echo rm climate_data.db
rm climate_data.db

echo python3 init_db.py
python3 init_db.py

echo python3 insert_db.py
python3 insert_db.py

echo python3 pre_computation.py
python3 pre_computation.py