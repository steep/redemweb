import os
import sqlite3
import redemLib

# Creation des courbes de premieres pages a afficher pour le site
#
#
#

def insert_curve(conn, cursor, id, curve_x, curve_y):
  for x, y in zip(curve_x, curve_y):
    data = {'id' : id, 'x' : x, 'y' : y}
    cursor.execute("""INSERT INTO CURVE(id, x, y)
                      VALUES(:id, :x, :y)""", data)
  
  conn.commit()


def insert_parameters(conn, cursor, id_data, free_mu, temperature, year_max, r, gamma, theta, free_sigma):
  data = {
    'id_data' : id_data,
    'free_mu' : free_mu,
    'temperature' : temperature,
    'year_max' : year_max,
    'r' : r,
    'gamma' : gamma,
    'theta' : theta,
    'free_sigma' : free_sigma
  }
  cursor.execute("""INSERT INTO PARAMETERS(id_data, free_mu, temperature, year_max, r, gamma, theta, free_sigma)
                    VALUES(:id_data, :free_mu, :temperature, :year_max, :r, :gamma, :theta, :free_sigma)""", data)
  
  conn.commit()


def insert_curve_for_overview(conn, cursor, db_file, data_name, step_bench):
  dataset = redemLib.DataSet(data_name)
  dataset.load_from_sqlite(db_file)
  
  temperature_l = dataset.get_possible_temperature()
  
  modelunfair = redemLib.Model(dataset)
  modelfair = redemLib.Model(dataset)
  
  zone = -1
  id_curve = 1
  
  for temp in temperature_l:
    modelunfair.configure_model(temp, 2030, 1.0, 1.0, 1000.0, 0.0) #### Unfair World
    
    (emission_unfair_x, emission_unfair_y) = modelunfair.compute_curves(zone, temp)
    insert_curve(conn, cursor, id_curve, emission_unfair_x, emission_unfair_y)
    
    id_curve = id_curve + 1
  
  for temp in temperature_l:
    (effort_unfair_x, effort_unfair_y) = modelunfair.compute_effort_tmax_benchmark_curve(zone, temp, step=step_bench)
    insert_curve(conn, cursor, id_curve, effort_unfair_x, effort_unfair_y)
    
    id_curve = id_curve + 1


def init_parameters_table(conn, cursor, db_file, data_name, tmax_l, r_l, gamma_l, theta_l, free_sigma_l):
  dataset = redemLib.DataSet(data_name)
  dataset.load_from_sqlite(db_file)
  
  model = redemLib.Model(dataset)
  
  temperature_l = dataset.get_possible_temperature()
  
  for temperature in temperature_l:
    for tmax in tmax_l:
      for r in r_l:
        for gamma in gamma_l:
          for theta in theta_l:
            for free_sigma in free_sigma_l:
              model.configure_model(temperature, tmax, r, gamma, theta, free_sigma)
              insert_parameters(conn, cursor, 1, model.free_mu, temperature, tmax, r, gamma, theta, free_sigma)


if __name__ == '__main__':
  conn = sqlite3.connect('climate_data.db')
  cursor = conn.cursor()

#### COMPLETE BUT SLOW
  tmax_l = list(range(2020, 2101, 5))
  r_l = [1.0]
  gamma_l = [1.0, 2.0]
  theta_l = [1000.0]
  free_sigma_l = [0.0, 5.0]

#### QUICK BUT UNCOMPLETE
#  tmax_l = list(range(2020, 2101, 30))
#  r_l = [1.0]
#  gamma_l = [1.0]
#  theta_l = [1000.0]
#  free_sigma_l = [0.0, 3.0]
  
  init_parameters_table(conn, cursor, 'climate_data.db', 'EDGAR', tmax_l, r_l, gamma_l, theta_l, free_sigma_l)
  
  insert_curve_for_overview(conn, cursor, 'climate_data.db', 'EDGAR', 5)
  
  conn.close()



































