from django.conf.urls import url

from . import views

app_name = 'redemCurve'
urlpatterns = [
  url(r'^([\w \s]*)$', views.redemCurve, name='redemCurve'),
]