from django.shortcuts import render
from django.http import HttpResponse, Http404
import redemLib
import json

DATABASE_FILE = 'climate_data.db'
UNFAIR_SIGMA = 0.0
FAIR_SIGMA = 5.0


web_page_content_language = {
  'English' : {
    'Index_Text' : '''
        <p>
        Welcome !<BR>
        This application allows you to better measure and understand the commitments made by the various
        States in the context of international climate conferences (in particular COP21 in Paris in 2016).
        </p>
        <p>
        It is aimed at
        </p>
        <UL>
        <LI>NGOs, media,</LI>
        <LI>individuals,</LI>
        <LI>stakeholders, institutions, decision-makers,...</LI>
        </UL>
        <p>          
        This web site allows you to visualize using intuitive curves
        </p>
        <UL>
        <LI>
                <p><STRONG>the gap between the commitments made and the objectives</STRONG> targeted and claimed,       </p>
        </LI>
        <LI>
                <p><STRONG>the extent of the additional efforts</STRONG> required.</p>
        </LI>
        </UL>
        <p>
         It may also be useful to researchers or more experienced users thanks to the specific tabs developed for "experts". 
        </p>
    ''',
    'Index_Button' : 'Use REDEM',
    'World' : 'World',
    'Equal' : 'Equal',
    'Differentiated' : 'Differentiated',
    'Emissions' : 'Emissions',
    'Year' : 'Year',
    'YDm' : 'YDm ',
    'Max_Decarbonization_label' : 'Max Decarbonization rate (in percentage)',
    'Decarbonization_label' : 'Decarbonization rate (in percentage)',
    'Temperature_label' : 'Temperature (in °C)',
    'Gap_label' : 'Gap between benchmark/INDC',
    'Reduction_emission_label' : 'Reductions of emissions',
    'COP21_INDC_label' : 'COP21 INDC',
    'No_INDC_label' : 'No INDC',
    'Optimal_INDC_Equal_label' : 'Optimal INDC Equal',
    'Optimal_INDC_Differentiated_label' : 'Optimal INDC Differentiated',
    'Optimistic' : 'Optimistic',
    'Pessimistic' : 'Pessimistic',
    'Overview' : 'Overview',
    'Visualize' : 'Visualize',
    'Effort_Visualization' : 'Effort Visualization',
    'Challenge_your_INDC' : 'Challenge your INDC',
    'All_Parameters' : 'All Parameters',
    'Expert_Version' : 'Expert Version',
    'Envelope' : 'Envelope',
    'Range' : 'Range',
    'Languages' : 'Languages',
    'RANGE_STEP' : 'STEP',
    'Show_INDC' : 'Show INDC',
    'Plot' : 'Plot',
    'Since' : 'Since',
    'Explanation_info_bulle' : 'Click me to have some explainations',
    'Modal_title_Overview' : 'Overview',
    'Modal_Overview' : '''
      <p>
        Welcome !<BR>
        This application allows you to better measure and understand the commitments made by the various
        States in the context of international climate conferences (in particular COP21 in Paris in 2016).
      </p>
    ''',
    'Modal_title_Visualize' : 'Visualize',
    'Modal_Visualize' : '''
    ''',
    'Modal_title_Effort_Visualisation' : 'Effort Visualization',
    'Modal_Effort_Visualisation' : '''
    ''',
    'Modal_title_Challenge_Your_INDC' : 'Challenge your INDC',
    'Modal_Challenge_Your_INDC' : '''
    ''',
    'Modal_title_Expert' : 'Expert Version',
    'Modal_Expert' : '''
    ''',
    'Modal_title_Envelope' : 'Envelope',
    'Modal_Envelope' : '''
    ''',
    'Modal_title_Range' : 'Parameter Range',
    'Modal_Range' : '''
    ''',
    'Overview_Description_1' : '''

        <B>What is it ?</B> <BR>
        These curves show scenarios of the evolution of <span class="style-highlightQuesaco">global greenhouse gas emissions</span> (all gases included)
        between 1990 and 2100 corresponding to different objectives for average temperature increase: 1.5°C, 2°C, 2.5°C, 2.5°C and 3°C. 
        The level of greenhouse gas emissions corresponding to the <span class="style-highlightQuesaco">"sum"</span> of the commitments made by
        the different countries at COP21 in Paris

        (<a href="#" class="info">INDC<span>
         INDCs = <EM>Intended Nationally Determined Contributions</EM><BR>
          = "voluntary" commitments to reduce gas emissions to<BR>
            greenhouse effect by 2025-2030 taken by the states at <BR>
            the COP21 to limit global warming.
        </span></a>) 
        is indicated in 2030 by a vertical mark colored here in <span style="color:#2679f6">blue</span> (interval).<BR>
        <BR>
        <B>Observations:</B> <BR>
        <span class="style-highlight">The commitments made by countries at COP21</span> on their level 
	emissions for 2030 <span class="style-highlight">are considerably insufficient. </span>
        They are not compatible with any of the trajectories:
        <span class="style-highlight">The gap between the 2030 levels of greenhouse gas emissions of 
	calculated curves and emission levels to which countries have committed themselves</span>  ("sum" of commitments made at COP21) 
	<span class="style-highlight">is colossal</span>, especially for objectives corresponding to an increase
	of 1.5°C and 2°C.

    ''',
    'Overview_Description_2' : '''
      <B>What is it ?</B> <BR>
        These curves indicate the <span class="style-highlightQuesaco">effort to be provided</span> (maximum value of the
        <a href="#" class="info">decarbonization rates<span>
           Decarbonization rate = decay rate of<BR>
           greenhouse gas emissions. 
           It is expressed as a percentage.
        </span></a>
        over the period 2010-2100)
        <span class="style-highlightQuesaco">to achieve the objective</span> of temperature increase targeted,
        <span class="style-highlightQuesaco">according to the action time</span> (date on which the maximum level of the decarbonization rate is reached).
        A curve is given for each of the objectives: 1.5°C, 2°C, 2.5°C, 2.5°C and 3°C.<BR>
        Note: It is possible to disable a curve by clicking on its legend. <BR>
        <BR>
        <B>Observations: </B> <BR>
        It does not matter what the temperature increase objective is,
        <span class="style-highlight">delaying its action irremediably leads to an increase in the maximum effort to be made. </span><BR>
        This growth is all the stronger as the target temperature increase is ambitious. <BR>
        <span class="style-highlight">It literally explodes with the objective of an increase to 1.5°C</span>, reaching decarbonization rates of more than 300% per year! <BR>
        Each year lost implies a multiplication of the efforts to be made in the following years! <BR>
        It should be noted here that, according to the literature on the subject,
        <span class="style-highlight">a decarbonization rate higher than 5% would lead to a collapse of
        the economy. </span>
    ''',
    'Visualize_Description_0' : '''
      <B>Instructions:</B>
        <span class="style-highlight">Select the desired countries</span> (or groups of countries) and the <span class="style-highlight">temperature increase target</span> target, then click on the "<span class="style-highlight">Plot</span>". <BR> button
        Click on the name of a region of the world to display or delete the list of associated countries. <BR>
        You can also modify the <a href="#" class="info">Peak Stress Date
        <span>Date at which the rate of decrease of the <BR>
              greenhouse gas emissions is maximum.
        </span></a>
        (<B>DPE</B>).

Translated with www.DeepL.com/Translator
    ''',
    'Visualize_Description_1' : '''
       <B>What is it ?</B> <BR>
        These curves show <span class="style-highlightQuesaco">benchmark scenarios for greenhouse gas emissions 
        for all selected countries</span> (or groups of countries) and <span class="style-highlightQuesaco">compatible with the selected temperature increase target</span> (over the period 1990-2100). <BR>
        <span class="style-highlightQuesaco">A left</span>, the curves correspond to scenarios for which the
        <a href="#" class="info">level of effort<span>
           The maximum value <BR>
           the rate of decrease in gas emissions with an effect of <BR>
           greenhouse over the period 2010-2100. <BR>
        </span></a>
        is forced to be the same for all countries. <BR>
        <span class="style-highlightQuesaco">A right</span>,
        the curves are those of scenarios in which
        the countries' efforts are differentiated according to a 
        <a href="#" class="info">indicator of "capacity" and "responsibility">indicator
           The indicator chosen here is the sum of GDP (Gross Domestic Product) <BR>
           with the level of CO2 emissions in 2010. <BR>
           GDP reflects the ability of countries to transform their economies<BR>
           towards a low-carbon economy, while the emission level of <BR>
           CO2 in 2000 are their responsibility in terms of<BR>
           global warming). 
        </span></a> :        
        The richer the country is and/or has already emitted a lot of greenhouse gases, the greater the effort required.
    
    ''',
    'Visualize_Description_2' : '''
         <B>What is it ?</B> <BR>
	These diagrams show the <span class="style-highlightQuesaco">gap between the INDCs and the reference emission curves</span> 
	("<EM>benchmark</EM>")
	associated with each of the temperature increase objectives considered (1.5°C, 2°C, 2.5°C, 2.5°C and 3°C). 
	These values depend on the
	<a href="#" class="info">peak effort date
        <span>Date at which the rate of decrease of the <BR>
              greenhouse gas emissions is maximum.
        </span></a> (ECD) which can be modified below. 
           The INDCs proposed at COP21 are sometimes packaged or require
	the use of scenarios to be converted into greenhouse gas emission levels in 2030. 
	Depending on the conditions and scenarios taken into account,
	<span class="style-highlightQuesaco">each INDC can vary by a minimum value 
	(optimistic INDC) to a maximum value (pessimistic INDC)</span>. </BR>
    ''',
    'Visualize_Description_3' : '''
         <BR>
        <B>Observations: </B> <BR>
        <span class="style-highlight"></span>
        <span class="style-highlight">Whatever the country, the commitments made at COP21</span> on emission levels (
        <a href="#" class="info">INDC<span>
         INDCs = <EM>Intended Nationally Determined Contributions</EM><BR>
          = voluntary' commitments to reduce gas emissions to <BR>
            greenhouse effect by 2025-2030 taken by the states at <BR>
            at COP21 to limit global warming.
        </span></a>
        ) 
        <span class="style-highlight">are totally incompatible</span> 
        <span class="style-highlight">with a global temperature increase target of 1.5°C</span>
        (objective signed in the agreement). <BR>
        <span class="style-highlight">These are considerably insufficient, even for an objective</span>
        of global temperature increase <span class="style-highlight">of 2°C</span>. <BR>
        For some developed countries or regions, such as Europe for example,
        the commitments made can be compatible with a global temperature increase of 2.5°C,
        but only when the level of effort is forced to be the same for all countries (left diagram).
        On the other hand, when efforts are differentiated (more "equitable" scenario; diagram on the right),
        <span class="style-highlight">the commitments of developed countries are still largely insufficient</span>,
        even with a temperature increase target of <span class="style-highlight">2.5°C</span>. <BR>
	It should also be noted that the reference curves of some countries such as China 
	(of which the COP21 INDC is insufficient even for a target of 2.5°C)     
 	are not very dependent on the chosen scenario. <BR>
        Finally, it should be noted that for some countries (such as India or China),
	the difference between pessimistic and optimistic INDC is extremely important.
	In these cases, the pessimistic INDC is very much above all the reference curves, even with a target of 3°C.
    ''',
    'Effort_Visualisation_Description_0' : '''
     <B>Instructions:</B>
        <span class="style-highlight">Select the desired country</span> (or group of countries), then <span class="style-highlight">click on the "Trace"</span>.<BR>
        You can also change the
        <a href="#" class="info">Peak Effort Date
        <span>Date at which the rate of decrease of the <BR>
              greenhouse gas emissions is maximum.
        </span></a>
         (ECD). <BR>
       <span class="style-green-color">The calculation time can be quite long. </span>
    
    ''',
    'Effort_Visualisation_Description_1' : '''
      <B>What is it ?</B> <BR>
        These diagrams indicate the
        <a href="#" class="info"><B>effort levels</B><span>
           These effort levels correspond to the maximum value <BR>
           the rate of decrease in gas emissions with an effect of <BR>
           greenhouse (decarbonization rate) over the period 2010-2100. <BR>
           It is expressed as a percentage.
        </span></a>
        <span>to be provided to achieve the different temperature increase objectives</span>,
        <span class="style-highlightQuesaco">according to these objectives</span>.<BR>
        These values are given for the selected country and for a
        <a href="#" class="info">peak effort date
        <span>Date at which the rate of decrease of the <BR>
              greenhouse gas emissions is maximum.
        </span></a>
        (DPE) set. <BR>
        <BR>
        The differences between the diagram on the left and the one on the right ("Equal" versus "Differentiated") are explained in the "View" tab. <BR> 
        <BR>
        <B>Observations: </B> <BR>
        <span class="style-highlight">
        The forces to be applied increase exponentially as a function of the temperature increase
        </span>
        set as a target.
        If, in undifferentiated scenarios, for example, the change from a target of 2.5°C to 2°C requires
        until the effort to be made doubles, it must be approximately multiplied
        by 6 or 7 to go from a target of 2°C to 1.5°C.
        <BR>
        These diagrams also allow, for example, to visualize that for some so-called "developed" countries,
        the transition from a first objective to a more ambitious objective is <B>proportionally</B> less
        expensive in a differentiated scenario than in an undifferentiated scenario (although in absolute terms, the efforts are greater).
        This further amplifies the "responsibility" of these countries in a scenario of a more "fair" world. 
    
    ''',
    'Effort_Visualisation_Description_2' : '''
      <B>What is it?</B> <BR>
        These curves indicate <span class="style-highlight">the effort to be made</span>
        to achieve a target temperature increase,
        <span class="style-highlight">according to the date of the peak effort</span> (ECD). <BR>
        In other words,
        they make it possible to measure the impact of the action time on the level of effort to be provided. <BR>
        These curves are given for the selected country (or group of countries) with scenarios
        where countries' levels of effort are differentiated and not differentiated. <BR>
        <span class="style-green-color">Note:</span> It is possible to disable a curve by clicking on its legend. <BR>
        <BR>
        <B>Observations: </B> <BR>
        Whatever the country considered and whatever the objective of temperature increase targeted,
        <span class="style-highlight">delaying its action irremediably leads to an increase in the maximum effort to be made. </span>
        As in the global case, this growth is all the stronger as the target for temperature increase is ambitious and
        <span class="style-highlight">explodes with the objective of a temperature increase to 1.5°C. </span><BR>
        <span class="style-highlight">These conclusions are true for all scenarios</span>whether the countries' levels of effort are differentiated or not.     
        <BR>
        It should be recalled here that, according to the scientific literature, a
        <a href="#" class="info">decarbonization rates<span>
           Decarbonization rate = rate of decrease in gas emissions at 
           greenhouse effect.
        </span></a>
        greater than 5% would result in a collapse of
        the economy. </BR>
        These curves therefore prove in all cases
        the urgency of the situation.
    
    ''',
    'Effort_Visualisation_Description_3' : '''
    
    ''',
    'Challenge_INDC_Description_0' : '''
    <B>Instructions:</B>
        Select the desired country, temperature and/or peak load date (PWD), then click on the "Plot" button. <BR>
        <By default, we recommend setting the peak effort date to 2030</B> so as to have the highest effort rates.
        as small as possible while remaining relatively realistic in terms of action times. 

    ''',
    'Challenge_INDC_Description_1' : '''
     The vast majority of the INDCs given at COP21 were given via a
       	<span class="style-highlightQuesaco">percent reduction in greenhouse gas emissions</span> between <span class="style-highlightQuesaco">2030</span>
        and a freely chosen reference year between 1990, 2005, 2010. <BR>
	<BR>
    ''',    
    'Challenge_INDC_Description_2' : '''
     <BR>
	Below, we indicate
	<span class="style-highlight">the percentage of emission reductions</span>
	corresponding 
	<span class="style-highlight">with reference curves</span>
	("<EM>benchmarks</EM>") between
	<span class="style-highlight">2030</span>
	and in those same years.
	In other words, we give here the <span class="style-highlightQuesaco">INDC theoretical ideals</span> of the selected country -or region 
	<span class="style-highlightQuesaco">allowing to be compatible with the selected temperature objective</span>. <BR>
	<BR>
	These values depend on the effort distribution scenario ("Equal" or "Differentiated") and the selected peak effort date. <BR>
	<BR>
    ''',    
    'Footer_Contact' : 'Contact us',
    'Footer_Authors' : 'Authors & Partners',
    'Footer_Credits' : 'Credits',
    'Footer_Download' : 'Download',
    'Modal_title_author' : 'Authors & Partners',
    'Modal_author' :'''
     Authors :
         <UL>
           <LI>Nicolas Assouad</LI>
           <LI><A HREF="https://fr.linkedin.com/in/h%25C3%25A9l%25C3%25A8ne-benveniste-a2150748">Hélène Benveniste</A>, CNRS, Institut Pierre Simon Laplace (IPSL)</LI>
           <LI><A HREF="http://emc3.lmd.jussieu.fr/en/group-members/oboucher?set_language=en">Olivier Boucher</A>, CNRS, Institut Pierre Simon Laplace (IPSL)</LI>
           <LI><A HREF="https://gael.univ-grenoble-alpes.fr/people/patrick-criqui?language=en">Patrick Criqui</A>, CNRS, Grenoble Applied Economics Lab (GAEL), axis Energy</LI>
           <LI><A HREF="https://www.encyclopedie-energie.org/auteur/ilasca-constantin/">Constantin Ilasca</A>, UGA, Grenoble Applied Economics Lab (GAEL), axis Energy</LI>
           <LI><A HREF="https://team.inria.fr/steep/people/emmanuel-prados/">Emmanuel Prados</A>, INRIA, project-team INRIA STEEP, Laboratoire Jean Kuntzmann (LJK)</LI>
         </UL>
         Partners :
          <UL>
           <LI>Laboratoires</LI>
            <UL>
             <LI><A href="https://gael.univ-grenoble-alpes.fr/research-areas/energy-axis?language=en">GAEL</A> (Grenoble Applied Economics Lab)</LI>
             <LI><A href="https://team.inria.fr/steep/">STEEP</A> (Sustainability Transition, Environment, Economy and local Policy) </LI>
             <LI><A href="http://www.ipsl.fr/en">IPSL</A> (Institut Pierre Simon Laplace)</LI>
             <LI><A href="http://www-ljk.imag.fr/index_en.php">LJK</A> (Laboratoire Jean Kuntzmann)</LI>
           </UL>
           <LI>Instituts</LI>
			  <CENTER>
			  <A href="https://team.inria.fr">
 			  <IMG src="/static/img/logo_INRIA.png" height=73  style="margin:0px 50px"></IMG></A>   
			  <A href="https://www.cnrs.fr/en">
 			  <IMG src="/static/img/logo-cnrs.jpg" height=73 style="margin:0px 50px"></IMG></A>     
			  <A href="https://gael.univ-grenoble-alpes.fr/research-areas/energy-axis?language=en">
 			  <IMG src="/static/img/logo-uga.png" height=73 style="margin:0px 50px"></IMG></A>     
			  <A href="http://www.lmd.jussieu.fr/">
 			  <IMG src="/static/img/upmc-logotype.gif" height=73 style="margin:0px 50px"></IMG></A>
			  </CENTER>           
         </UL>
    ''',
    'Modal_title_credit' : '',
    'Modal_credit' : '',
    'Modal_title_download' : '',
    'Modal_download' : '',
    'Modal_title_contact' : '',
    'Modal_contact' : '''
    To contact us,
         send an email to <A HREF="mailto:redem@inria.fr">redem@inria.fr</A>
         or contact one of the authors directly:
         <UL>
           <LI><A HREF="http://emc3.lmd.jussieu.fr/en/group-members/oboucher">Olivier Boucher</A></LI>
           <LI><A HREF="https://gael.univ-grenoble-alpes.fr/people/patrick-criqui&language=en">Patrick Criqui</A></LI>
           <LI><A HREF="https://team.inria.fr/steep/people/emmanuel-prados/">Emmanuel Prados</A></LI>
         </UL>
    '''
  },
  'French' : {
    'Index_Text' : '''
        <p>
        Bienvenue !<BR>
        Cette application vous permet de mieux mesurer et appréhender les engagements pris par les différents
        états dans le cadre des conférences internationales sur le climat (en particulier la COP21 de Paris en 2016).
        </p>
        <p>
        Elle s'adresse aux
        </p>
        <UL>
        <LI>ONG, médias,</LI>
        <LI>particuliers,</LI>
        <LI>parties prenantes, institutions, décideurs, ...</LI>
        </UL>
        <p>          
        Ce site permet de visualiser à l'aide de courbes intuitives
        </p>
        <UL>
        <LI>
                <p><STRONG>le décalage entre les engagements pris et les objectifs</STRONG> visés et affirmés,       </p>
        </LI>
        <LI>
                <p><STRONG>l'ampleur des efforts supplémentaires</STRONG> à fournir.       </p>
        </LI>
        </UL>
        <p>
         Il pourra aussi être utile aux chercheurs ou utilisateurs plus avertis grâce aux onglets spécifiques développés pour les "experts". 
        </p>
        ''',
    'Index_Button' : 'Utiliser REDEM',
    'World' : 'Monde',
    'Equal' : 'Egal',
    'Differentiated' : 'Différencié',
    'Emissions' : 'Emissions',
    'Year' : 'Année',
    'YDm' : 'DPE',
    'Max_Decarbonization_label' : 'Taux de décarbonisation Max (en pourcentage)',
    'Decarbonization_label' : 'Taux de décarbonisation (en pourcentage)',
    'Temperature_label' : 'Température (en °C)',
    'Gap_label' : 'Différence entre benchmark/INDC',
    'Reduction_emission_label' : 'Réductions des émissions',
    'COP21_INDC_label' : 'INDC COP21',
    'No_INDC_label' : 'Aucun INDC',
    'Optimal_INDC_Equal_label' : 'INDC idéal théorique - scénario avec effort "Egal"',
    'Optimal_INDC_Differentiated_label' : 'INDC idéal théorique - scénario avec effort "Différencié"',
    'Optimistic' : ' - INDC optimiste',
    'Pessimistic' : '- INDC pessimiste',
    'Overview' : 'Aperçu',
    'Visualize' : 'Visualisation',
    'Effort_Visualization' : 'Visualisation de l\'Effort',
    'Challenge_your_INDC' : 'Confrontez votre INDC',
    'All_Parameters' : 'Tous les Paramètres',
    'Expert_Version' : 'Version Expert',
    'Envelope' : 'Enveloppe',
    'Range' : 'Variation de Paramètre',
    'Languages' : 'Langages',
    'RANGE_STEP' : 'PAS',
    'Show_INDC' : 'Afficher les INDC',
    'Plot' : 'Tracer',
    'Since' : 'Depuis ',
    'Explanation_info_bulle' : 'Cliquez ici pour obtenir plus d\'informations',
    'Modal_title_Overview' : 'Aperçu',
    'Modal_Overview' : '''
     <p>
        REDEM<BR>
        Cette application vous permet de mieux mesurer et appréhender les engagements pris par les différents
        états dans le cadre des conférences internationales sur le climat (en particulier la COP21 de Paris en 2016).
      </p>
    ''',
    'Modal_title_Visualize' : 'Visualisation',
    'Modal_Visualize' : '''
    ''',
    'Modal_title_Effort_Visualisation' : 'Visualisation de l\'Effort',
    'Modal_Effort_Visualisation' : '''
    ''',
    'Modal_title_Challenge_Your_INDC' : 'Confrontez votre INDC',
    'Modal_Challenge_Your_INDC' : '''
    ''',
    'Modal_title_Expert' : 'Version Expert',
    'Modal_Expert' : '''
    ''',
    'Modal_title_Envelope' : 'Enveloppe',
    'Modal_Envelope' : '''
    ''',
    'Modal_title_Range' : 'Variation de Paramètre',
    'Modal_Range' : '''
    ''',
    'Overview_Description_1' : '''        
        <B>Quèsaco ?</B> <BR>
        Ces courbes montrent des scénarios d'évolution des <span class="style-highlightQuesaco">émissions mondiales de gaz à effet de serre</span> (tout gaz compris)
        entre 1990 et 2100 correspondant à différents objectifs d'augmentation de température moyenne : 1.5°C, 2°C, 2.5°C et 3°C. 
        Le niveau d'émission de gaz à effet de serre correspondant à la <span class="style-highlightQuesaco">“somme”</span> des engagements pris par
        les différents pays lors de la COP21 à Paris
        (<a href="#" class="info">INDC<span>
         INDCs = <EM>Intended Nationally Determined Contributions</EM><BR>
          = engagements "volontaires" de réduction d’émissions de gaz à <BR>
            effet de serre d’ici 2025-2030 pris par les états lors de <BR>
            la COP21 afin de limiter le réchauffement de la planète.
        </span></a>) 
        est indiqué en 2030 par une marque verticale colorée ici en <span style="color:#2679f6">bleu</span> (intervalle).<BR>
        <BR>
        <B>Constat :</B> <BR>
        <span class="style-highlight">Les engagements pris par les pays lors de la COP21</span> sur leur niveau 
	d’émission pour 2030 <span class="style-highlight">sont considérablement insuffisants.</span>
        Ils ne sont compatibles avec aucunes des trajectoires : 
        <span class="style-highlight">L’écart entre les niveaux en 2030 des émissions de gaz à effet de serre des 
	courbes calculées et les niveaux d'émission sur lesquels les pays se sont engagés</span>  (“somme” des engagements pris lors de la COP21) 
	<span class="style-highlight">est colossal</span>, en particulier pour les objectifs correspondant à une augmentation
	de température globale de 1.5°C et 2°C.
	''',
    'Overview_Description_2' : '''
        <B>Quèsaco ?</B> <BR>
        Ces courbes indiquent l'<span class="style-highlightQuesaco">effort à fournir</span> (valeur maximale du
        <a href="#" class="info">taux de décarbonisation<span>
           Taux de décarbonisation = taux de décroissance des<BR>
           émissions de gaz à effet de serre. 
           Il est exprimé en pourcentage.
        </span></a>
        sur la période 2010-2100)
        <span class="style-highlightQuesaco">pour atteindre l'objectif</span>  d'augmentation de température visé,
        <span class="style-highlightQuesaco">en fonction du délais d'action</span>  (date à laquelle le niveau maximum du taux de décarbonisation est atteint).
        Une courbe est donnée pour chacun des objectifs : 1.5°C, 2°C, 2.5°C et 3°C.<BR>
        Note : Il est possible de désactiver une courbe en cliquant sur sa légende.<BR>
        <BR>
        <B>Constat :</B> <BR>
        Peu importe l'objectif d'augmentation de température visé,
        <span class="style-highlight">le fait de retarder son action entraine irrémédiablement une hausse de l'effort maximal à fournir.</span><BR>
        Cette croissance est d'autant plus forte que l'objectif d'augmentation de température visé est ambitieux.<BR>
        <span class="style-highlight">Elle explose littéralement avec l'objectif d’une augmentation à 1.5°C</span>, atteignant des taux de décarbonisation de plus de 300 % par an ! <BR>
        Chaque année perdue implique une démultiplication des efforts à fournir les années suivantes ! <BR>
        Notons ici que, d'après la littérature consacrée,
        <span class="style-highlight">un taux de décarbonisation supérieur à 5 % engendrerait un effondrement de
        l'économie.</span>
    ''',
    'Visualize_Description_0' : '''
        <B>Instructions :</B>
        <span class="style-highlight"> Sélectionnez les pays</span> (ou groupes de pays) désirés et l'<span class="style-highlight">objectif d'augmentation de température</span> visé, puis cliquez sur le bouton "<span class="style-highlight">Tracer</span>".<BR>
        Cliquer sur le nom d'une région du monde pour faire apparaître ou disparaître la liste des pays associés.<BR>
        Vous pouvez aussi modifier la <a href="#" class="info">Date du Pic d'Effort
        <span>Date à laquelle le taux de décroissance des <BR>
              émissions de gaz à effet de serre est maximal.
        </span></a>
        (<B>DPE</B>).
   ''',
    'Visualize_Description_1' : '''
        <B>Quèsaco ?</B> <BR>
        Ces courbes montrent des <span class="style-highlightQuesaco">scénarios de référence des émissions de gaz à effet de serre 
        pour tous les pays selectionnés</span> (ou groupes de pays) et <span class="style-highlightQuesaco">compatibles avec l'objectif d'augmentation de température selectionné</span> (sur la période 1990-2100). <BR>
        <span class="style-highlightQuesaco">A gauche</span>, les courbes correspondent à des scénarios pour lesquels le
        <a href="#" class="info">niveau d'effort<span>
           On appelle ici "niveau d'effort" la valeur maximale <BR>
           du taux de décroissance des émissions de gaz à effet de <BR>
           serre sur la période 2010-2100. <BR>
        </span></a>
        est forcé à être le même pour tous les pays.<BR>
        <span class="style-highlightQuesaco">A droite</span>,
        les courbes sont celles de scénarios dans lesquels
        les efforts des pays sont differentiés selon un 
        <a href="#" class="info">indicateur de "capacité" et de "responsabilité"<span>
           L'indicateur choisi ici est la somme du PIB (Produit Interieur Brut) <BR>
           avec le niveau d'émission de CO2 en 2010. <BR>
           Le PIB traduit la capacité des pays a transformer leur économie<BR>
           vers une économie décarbonée, alors que le niveau d'émission de <BR>
           CO2 en 2000 figurent leur responsabilité en terme de<BR>
           rechauffement climatique). 
        </span></a>  :        
        Plus le pays est riche et/ou a déja émit beaucoup de gaz à effet de serre, plus l'effort à fournir est important.
    ''',
    'Visualize_Description_2' : '''
       <B>Quèsaco ?</B> <BR>
	Ces diagrammes montrent l'<span class="style-highlightQuesaco">écart qu'il y a entre les INDC et les courbes d'émission de référence</span> 
	(“<EM>benchmark</EM>”)
	associées à  chacun des objectifs d'augmentation de température considérés (1.5°C, 2°C, 2.5°C et 3°C). 
	Ces valeurs dépendent de la
	<a href="#" class="info">date du pic d'effort
        <span>Date à laquelle le taux de décroissance des <BR>
              émissions de gaz à effet de serre est maximal.
        </span></a> (DPE) qui peut être modifiée ci-dessous. 
           Les INDC proposés à l’occasion de la COP21 sont parfois conditionnés ou nécessitent
	l’utilisation de scénarios pour être convertis en niveau d’émissions de gaz à effet de serre en 2030. 
	Selon les conditions et scénarios pris en compte,
	<span class="style-highlightQuesaco">chaque INDC peut varier d’une valeur minimale 
	(INDC optimiste) à une valeur maximale (INDC pessimiste)</span>. </BR>
    ''',
    'Visualize_Description_3' : '''
        <BR>
        <B>Constat :</B> <BR>
        <span class="style-highlight"></span>
        <span class="style-highlight">Quelque soit le pays, les engagements pris lors la COP21</span> sur les niveaux d'émission (
        <a href="#" class="info">INDC<span>
         INDCs = <EM>Intended Nationally Determined Contributions</EM><BR>
          = engagements "volontaires" de réduction d’émissions de gaz à <BR>
            effet de serre d’ici 2025-2030 pris par les états lors de <BR>
            la COP21 afin de limiter le réchauffement de la planète.
        </span></a>
        ) 
        <span class="style-highlight">sont totalement incompatibles</span> 
        <span class="style-highlight">avec un objectif d'augmentation de température globale de 1.5°C</span>
        (objectif signé dans l’accord).<BR>
        <span class="style-highlight">Ceux-ci sont considérablement insuffisants, même pour un objectif</span>
        d'augmentation de température globale <span class="style-highlight">de 2°C</span>. <BR>
        Pour certaines régions ou pays développés, comme l'Europe par exemple,
        les engagements pris peuvent être compatibles avec une augmentation de température globale de 2.5°C,
        mais seulement lorsque le niveau d'effort est forcé à être le même pour tous les pays (diagramme de gauche).
        Par contre, lorsque les efforts sont différenciés (scénario plus “équitable” ; diagramme de droite),
        <span class="style-highlight">les engagements des pays développés restent toujours largement insuffisants</span>,
        même avec un objectif d'augmentation de température à <span class="style-highlight">2.5°C</span>. <BR>
	Notons par ailleurs que les courbes de référence de certains pays comme la Chine 
	(dont l'INDC de la COP21 est insuffisant même pour un objectif de 2.5°C)     
 	sont peu dépendants du scénario choisi.<BR>
        Notons enfin que pour certain pays (comme par exemple l’Inde ou encore la Chine),
	la différence entre l’INDC pessimiste et l’INDC optimiste est extrêmement important.
	Dans ces cas là, l'INDC pessimiste est très largement au dessus de toutes les courbes de référence, même avec un objectif de 3°C.
    ''',
    'Effort_Visualisation_Description_0' : '''
        <B>Instructions :</B>
        <span class="style-highlight">Sélectionnez le pays</span> (ou groupe de pays) désiré, puis <span class="style-highlight">cliquez sur le bouton "Tracer"</span>.<BR>
        Vous pouvez aussi modifier la
        <a href="#" class="info">Date du Pic d'Effort
        <span>Date à laquelle le taux de décroissance des <BR>
              émissions de gaz à effet de serre est maximal.
        </span></a>
         (DPE).<BR>
       <span class="style-green-color">Le temps de calcul peut être assez long.</span>
    ''',
    'Effort_Visualisation_Description_1' : '''
        <B>Quèsaco ?</B> <BR>
        Ces diagrammes indiquent les
        <a href="#" class="info"><B>niveaux d'effort</B><span>
           Ces niveaux d'effort correspondent à la valeur maximale <BR>
           du taux de décroissance des émissions de gaz à effet de <BR>
           serre (taux de décarbonisation) sur la période 2010-2100. <BR>
           Il est exprimé en pourcentage.
        </span></a>
        <span>à fournir pour atteindre les différents objectifs d'augmentation de température</span>,
        <span class="style-highlightQuesaco">en fonction de ces objectifs</span>.<BR>
        Ces valeurs sont données pour le pays selectionné et pour une
        <a href="#" class="info">date du pic d'effort
        <span>Date à laquelle le taux de décroissance des <BR>
              émissions de gaz à effet de serre est maximal.
        </span></a>
        (DPE) fixée. <BR>
        <BR>
        Les différences entre le diagramme de gauche et celui de droite ("Egale" versus "Différencié") sont expliquées dans l'onglet "Visualisation".<BR> 
        <BR>
        <B>Constat :</B> <BR>
        <span class="style-highlight">
        Les efforts à fournir augmentent de manière exponentielle en fonction de l'augmentation de température
        </span>
        fixée en objectif.
        Si, dans les scénarios non différenciés par exemple,  le passage d'un objectif de 2.5°C à 2°C nécessite
        jusqu'à doubler l'effort à fournir, celui-ci doit être approximativement multiplier
        par 6 ou 7 pour passer d'un objectif de 2°C à 1.5°C.
        <BR>
        Ces diagrammes permettent aussi, par exemple, de visualiser que pour certains pays dits "développés",
        le passage d'un premier objectif à un objectif plus ambitieux est <B>proportionnellement</B> moins
        couteux dans un scénario différencié que dans un scénario non-différencié (bien qu'en valeur absolue, les efforts soient plus importants).
        Ceci amplifie d'autant plus la "responsabilité" de ces pays dans un scenario d'un monde plus "juste". 
           ''',
    'Effort_Visualisation_Description_2' : '''
        <B>Quèsaco ?</B> <BR>
        Ces courbes indiquent <span class="style-highlight">l'effort à fournir</span>
        pour atteindre un objectif d'augmentation de température visé,
        <span class="style-highlight">en fonction de la date du pic d'effort</span> (DPE).<BR>
        Autrement dit,
        elles permettent de mesurer l'impact du délais d'action sur le niveau d'effort à fournir.<BR>
        Ces courbes sont données pour le pays (ou groupe de pays) sélectionné avec des scénarios
        où les niveaux d'effort des pays sont differenciés et non differenciés.<BR>
        <span class="style-green-color">Note :</span> Il est possible de désactiver une courbe en cliquant sur sa légende.<BR>
        <BR>
        <B>Constat :</B> <BR>
        Quelque soit le pays considéré et peu importe l'objectif d'augmentation de température visé,
        <span class="style-highlight">le fait de retarder son action entraine irrémédiablement une hausse de l'effort maximal à fournir.</span>
        Comme dans le cas mondial, cette croissance est d'autant plus forte que l'objectif d'augmentation de température visé est ambitieux et
        <span class="style-highlight">explose avec l'objectif d’une augmentation de température à 1.5°C.</span><BR>
        <span class="style-highlight">Ces conclusions se vérifient pour tous les scénarios</span> que les niveaux d'effort des pays soient differenciés ou non.     
        <BR>
        Rappelons ici que, d'après la littérature scientifique, un
        <a href="#" class="info">taux de décarbonisation<span>
           Taux de décarbonisation = taux de décroissance des émissions de gaz à 
           effet de serre.
        </span></a>
        supérieur à 5 % engendrerait un effondrement de
        l'économie.</BR>
        Ces courbes prouvent donc dans tous les cas
        l'urgence de la situation.
    ''',
    'Effort_Visualisation_Description_3' : '''

    ''',
    'Challenge_INDC_Description_0' : '''
        <B>Instructions :</B>
        Sélectionnez le pays, la température et/ou la date du pic d'effort (DPE) désirés, puis cliquez sur le bouton "Tracer". <BR>
        <B>Par défaut, nous conseillons de fixer  la date du pic d’effort à 2030</B> de façon à avoir des taux d’effort les
        plus petits possibles tout en restant relativement réaliste en terme de délais d’action.        
    ''',
    'Challenge_INDC_Description_1' : '''
       	La grande majorité des INDC donnés lors de la COP21 l’ont  été via un
       	<span class="style-highlightQuesaco">pourcentage de réduction des émissions</span> de gaz à effet de serre entre <span class="style-highlightQuesaco">2030</span>
        et une année de référence librement choisie entre 1990, 2005, 2010. <BR>
	<BR>
	     ''',
    'Challenge_INDC_Description_2' : '''
	<BR>
	Ci-dessous, nous indiquons
	<span class="style-highlight">le pourcentage de réductions des émissions</span>
	correspondant 
	<span class="style-highlight">aux courbes de référence</span>
	("<EM>benchmarks</EM>") entre
	<span class="style-highlight">2030</span>
	et ces mêmes années.
	En d’autre termes, nous donnons ici les <span class="style-highlightQuesaco">INDC idéaux théoriques</span> du pays -ou région- sélectionné 
	<span class="style-highlightQuesaco">permettant d’être compatible avec l’objectif de température sélectionné</span>. <BR>
	<BR>
	Ces valeurs dépendent du scénario de répartition des efforts (“Egal” ou “Différencié”) ainsi que de la date du pic d’effort sélectionnée.<BR>
	<BR>
	     ''',
    'Footer_Contact' : 'Nous contacter',
    'Footer_Authors' : 'Auteurs & Partenaires',
    'Footer_Credits' : 'Crédits',
    'Footer_Download' : 'Télécharger',
    'Modal_title_author' : 'Auteurs & Partenaires',
    'Modal_author' : ''' 
         Auteurs :
    <UL>
           <LI>Nicolas Assouad</LI>
           <LI><A HREF="https://fr.linkedin.com/in/h%25C3%25A9l%25C3%25A8ne-benveniste-a2150748">Hélène Benveniste</A>, CNRS, Institut Pierre Simon Laplace (IPSL)</LI>
           <LI><A HREF="http://emc3.lmd.jussieu.fr/en/group-members/oboucher">Olivier Boucher</A>, CNRS, Institut Pierre Simon Laplace (IPSL)</LI>
           <LI><A HREF="https://gael.univ-grenoble-alpes.fr/membres/patrick-criqui">Patrick Criqui</A>, CNRS, Laboratoire d"Economie Appliquée de Grenoble (GAEL), axe Energie</LI>
           <LI><A HREF="https://www.encyclopedie-energie.org/auteur/ilasca-constantin/">Constantin Ilasca</A>, UGA, Laboratoire d"Economie Appliquée de Grenoble (GAEL), axe Energie</LI>
           <LI><A HREF="https://team.inria.fr/steep/people/emmanuel-prados/">Emmanuel Prados</A>, INRIA, Equipe-projet INRIA STEEP, Laboratoire Jean Kuntzmann (LJK)</LI>
         </UL>

         Partenaires :
          <UL>
           <LI>Laboratoires</LI>
            <UL>
             <LI><A href="https://gael.univ-grenoble-alpes.fr/">GAEL</A> (Laboratoire d'Economie Appliquée de Grenoble)</LI>
             <LI><A href="https://team.inria.fr/steep/">STEEP</A> (Sustainability Transition, Environment, Economy and local Policy) </LI>
             <LI><A href="http://www.ipsl.fr">IPSL</A> (Institut Pierre Simon Laplace)</LI>
             <LI><A href="http://www-ljk.imag.fr/">LJK</A> (Laboratoire Jean Kuntzmann)</LI>
           </UL>
           <LI>Instituts</LI>
			  <CENTER>
			  <A href="https://team.inria.fr">
 			  <IMG src="https://www.inria.fr/extension/site_inria/design/site_inria/images/logos/logo_INRIA.png" height=73  style="margin:0px 50px"></IMG></A>   
			  <A href="http://www.cnrs.fr">
 			  <IMG src="http://www.cnrs.fr/fr/z-tools/newune/themes/CNRSTheme/images/logocnrs.png" height=73 style="margin:0px 50px"></IMG></A>     
			  <A href="http://www.univ-grenoble-alpes.fr">
 			  <IMG src="https://gael.univ-grenoble-alpes.fr/sites/gael/themes/ujf_gael/images/logo_uga.jpeg" height=73 style="margin:0px 50px"></IMG></A>     
			  <A href="http://www.lmd.jussieu.fr/">
 			  <IMG src="http://www.upmc.fr/skins/UPMC/templates/index/resources/img/upmc-logotype.gif" height=73 style="margin:0px 50px"></IMG></A>
			  </CENTER>           
         </UL>
     ''',
    'Modal_title_credit' : 'Crédits',
    'Modal_credit' : '',
    'Modal_title_download' : 'Téléchargement',
    'Modal_download' : '',
    'Modal_title_contact' : 'Nous contacter',
    'Modal_contact' : '''
         Pour nous contacter,
         envoyer un email a <A HREF="mailto:redem@inria.fr">redem@inria.fr</A>
         ou contacter directement un des auteurs :

         <UL>
           <LI><A HREF="http://emc3.lmd.jussieu.fr/en/group-members/oboucher">Olivier Boucher</A></LI>
           <LI><A HREF="https://gael.univ-grenoble-alpes.fr/membres/patrick-criqui">Patrick Criqui</A></LI>
           <LI><A HREF="https://team.inria.fr/steep/people/emmanuel-prados/">Emmanuel Prados</A></LI>
         </UL>
    ''',
  }
}

def get_text(dic_content, text, language):
  return dic_content[language].get(text, dic_content['English'][text])


def get_web_page_content(dic_content, language):
  out = {}
  for key in dic_content['English'].keys():
    out[key] = get_text(dic_content, key, language)
  
  return out


def convert_curve_for_display(x, y):
  out = []
  for (elem_x, elem_y) in zip(x, y):
    out.append([elem_x, round(elem_y, 2)])
  return out

# Create your views here.
def index(request, language):
  
  if language == '':
    language = 'English'
  
  web_page_content = get_web_page_content(web_page_content_language, language)
  
  return render(request, 'redemCurve/index.html', locals())


def init_output_request():
  return {
    'CHARTS1' : {},
    'CHARTS2' : {},
    'CHARTS3' : {},
    'CHARTS4' : {},
    'CHARTS1_TITLE' : '',
    'CHARTS2_TITLE' : '',
    'CHARTS3_TITLE' : '',
    'CHARTS4_TITLE' : '',
    'CHARTS1_X_LABEL' : '',
    'CHARTS2_X_LABEL' : '',
    'CHARTS3_X_LABEL' : '',
    'CHARTS4_X_LABEL' : '',
    'CHARTS1_Y_LABEL' : '',
    'CHARTS2_Y_LABEL' : '',
    'CHARTS3_Y_LABEL' : '',
    'CHARTS4_Y_LABEL' : '',
  }


def remove_prefix(zone):
  if zone.startswith('zone_'):
    return int(zone[len('zone_'):])
  else:
    return None


def get_list_zone(request_data):
  zone_list = []
  
  for zone in request_data.keys():
    id_zone = remove_prefix(zone)
    if id_zone and request_data[zone] == "true":
      zone_list.append(id_zone)
  
  return zone_list


def get_INDC(model, id_zone):
  country_indc = model.dataset.get_indc_of_zone(id_zone)
    
  if country_indc != None:
    (indc_year, indc_min, indc_max) = country_indc
    return [[indc_year, indc_min], [indc_year, indc_max]]
  
  return None


def get_name_world(language):
  return get_text(web_page_content_language, 'World', language)


def get_name_of_zone(id_zone, language, dataset):
  if id_zone!=-1:
    return dataset.get_name_of_zone(id_zone, language)
  else:
    return get_name_world(language)


def get_percentage_indc(dataset, id_zone, indc_carbon, year, temperature):
  emissions = dataset.get_emissions_of_zone(id_zone, year, temperature)
  return round(100 * (1 - indc_carbon / emissions), 2)


def OVERVIEW_request(request_data, data_name, language):
  
  sql_interface = redemLib.SQLite_Climate_Interface(DATABASE_FILE, data_name)
  curves = sql_interface.load_curves()
  indc_l = sql_interface.load_indc()
  
  zone = get_name_world(language)
  
  out = init_output_request()
  
  out['CHARTS1_TITLE'] = zone
  out['CHARTS1_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS1_Y_LABEL'] = get_text(web_page_content_language, 'Emissions', language) + ' (kton CO2(eq))'
  
  out['CHARTS2_TITLE'] = zone
  out['CHARTS2_X_LABEL'] = get_text(web_page_content_language, 'YDm', language)
  out['CHARTS2_Y_LABEL'] = get_text(web_page_content_language, 'Max_Decarbonization_label', language)
  
  
  (emission_15_x, emission_15_y) = curves[1]
  (emission_2_x, emission_2_y) = curves[2]
  (emission_25_x, emission_25_y) = curves[3]
  (emission_3_x, emission_3_y) = curves[4]
  
  (effort_15_x, effort_15_y) = curves[5]
  (effort_2_x, effort_2_y) = curves[6]
  (effort_25_x, effort_25_y) = curves[7]
  (effort_3_x, effort_3_y) = curves[8]
  
  indc = indc_l[-1]
  indc = [[indc[0], indc[1]], [indc[0], indc[2]]]
  
  out['CHARTS1'][zone + ' 1.5°C'] = convert_curve_for_display(emission_15_x, emission_15_y)
  out['CHARTS1'][zone + ' 2.0°C'] = convert_curve_for_display(emission_2_x, emission_2_y)
  out['CHARTS1'][zone + ' 2.5°C'] = convert_curve_for_display(emission_25_x, emission_25_y)
  out['CHARTS1'][zone + ' 3.0°C'] = convert_curve_for_display(emission_3_x, emission_3_y)
  out['CHARTS1'][zone + ' INDC'] = indc
  out['CHARTS2'][zone + ' 1.5°C'] = convert_curve_for_display(effort_15_x, effort_15_y)
  out['CHARTS2'][zone + ' 2.0°C'] = convert_curve_for_display(effort_2_x, effort_2_y)
  out['CHARTS2'][zone + ' 2.5°C'] = convert_curve_for_display(effort_25_x, effort_25_y)
  out['CHARTS2'][zone + ' 3.0°C'] = convert_curve_for_display(effort_3_x, effort_3_y)
  
  return out


def VISUALIZE_request(request_data, data_name, language):
  tmax = int(request_data['tmax'])
    
  temperature = float(request_data['temperature'])
  
  indc_required = request_data['show_indc']
  
  dataset = redemLib.DataSet(data_name)
  dataset.load_from_sqlite(DATABASE_FILE)
  
  modelunfair = redemLib.Model(dataset)
  modelfair = redemLib.Model(dataset)
  
  modelunfair.configure_model(temperature, tmax, 1.0, 1.0, 1000.0, UNFAIR_SIGMA) #### Unfair World
  modelfair.configure_model(temperature, tmax, 1.0, 1.0, 1000.0, FAIR_SIGMA) #### Fair World
  
  out = init_output_request()
  
  out['CHARTS1_TITLE'] = get_text(web_page_content_language, 'Equal', language) + ' : ' + str(temperature) + '°C'
  out['CHARTS1_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS1_Y_LABEL'] = get_text(web_page_content_language, 'Emissions', language) + ' (' + modelunfair.dataset.data['units']['emission'] + ')'
  
  out['CHARTS2_TITLE'] = get_text(web_page_content_language, 'Differentiated', language) + ' : ' + str(temperature) + '°C'
  out['CHARTS2_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS2_Y_LABEL'] = get_text(web_page_content_language, 'Emissions', language) + ' (' + modelunfair.dataset.data['units']['emission'] + ')'
  
  out['CHARTS3_TITLE'] = get_text(web_page_content_language, 'Equal', language) + ' : ' + get_text(web_page_content_language, 'YDm', language) + '=' + str(tmax)
  out['CHARTS3_X_LABEL'] = get_text(web_page_content_language, 'Temperature_label', language)
  out['CHARTS3_Y_LABEL'] = get_text(web_page_content_language, 'Gap_label', language) + ' (' + modelunfair.dataset.data['units']['emission'] +')'
  
  out['CHARTS4_TITLE'] = get_text(web_page_content_language, 'Differentiated', language) + ' : ' + get_text(web_page_content_language, 'YDm', language) + '=' + str(tmax)
  out['CHARTS4_X_LABEL'] = get_text(web_page_content_language, 'Temperature_label', language)
  out['CHARTS4_Y_LABEL'] = get_text(web_page_content_language, 'Gap_label', language) + ' (' + modelunfair.dataset.data['units']['emission'] +')'
  
  zone_list = get_list_zone(request_data)
  zone_list_benchmark = []
    
  for id_zone in zone_list:
    zone = get_name_of_zone(id_zone, language, dataset)
    (emission_unfair_x, emission_unfair_y) = modelunfair.compute_curves(id_zone, temperature)
    (emission_fair_x, emission_fair_y) = modelfair.compute_curves(id_zone, temperature)
    
    out['CHARTS1'][zone] = convert_curve_for_display(emission_unfair_x, emission_unfair_y)
    out['CHARTS2'][zone] = convert_curve_for_display(emission_fair_x, emission_fair_y)
    
    if indc_required == "true":
      indc = get_INDC(modelunfair, id_zone)
      if indc != None:
        zone_list_benchmark.append(id_zone)
        out['CHARTS1'][zone + ' INDC'] = indc
        out['CHARTS2'][zone + ' INDC'] = indc
  
  if indc_required == "true" and len(zone_list_benchmark) != 0:
    (indc_benchmark_unfair_x, indc_benchmark_unfair_y_dic) = modelunfair.compute_indc_benchmark_curve(zone_list_benchmark)
    (indc_benchmark_fair_x, indc_benchmark_fair_y_dic) = modelfair.compute_indc_benchmark_curve(zone_list_benchmark)
    
    label_min =  ' ' + get_text(web_page_content_language, 'Optimistic', language)
    label_max =  ' ' + get_text(web_page_content_language, 'Pessimistic', language)
  
    for id_zone, (indc_min, indc_max) in indc_benchmark_unfair_y_dic.items():
      zone = get_name_of_zone(id_zone, language, dataset)
      out['CHARTS3'][zone] = [
        (convert_curve_for_display(indc_benchmark_unfair_x, indc_min), zone + label_min),
        (convert_curve_for_display(indc_benchmark_unfair_x, indc_max), zone + label_max),
      ]
    
    for id_zone, (indc_min, indc_max) in indc_benchmark_fair_y_dic.items():
      zone = get_name_of_zone(id_zone, language, dataset)
      out['CHARTS4'][zone] = [
        (convert_curve_for_display(indc_benchmark_fair_x, indc_min), zone + label_min),
        (convert_curve_for_display(indc_benchmark_fair_x, indc_max), zone + label_max)
      ]
  
  return out


def EFFORT_VISUALIZATION_request(request_data, data_name, language):
  
  tmax = int(request_data['tmax'])
  temperature = float(request_data['temperature'])
  zone = request_data['zone']
  
  dataset = redemLib.DataSet(data_name)
  dataset.load_from_sqlite(DATABASE_FILE)
  
  modelunfair = redemLib.Model(dataset)
  modelfair = redemLib.Model(dataset)
  
  modelunfair.configure_model(temperature, tmax, 1.0, 1.0, 1000.0, UNFAIR_SIGMA) #### Unfair World
  modelfair.configure_model(temperature, tmax, 1.0, 1.0, 1000.0, FAIR_SIGMA) #### Fair World
  
  id_zone = remove_prefix(zone)
  zone = get_name_of_zone(id_zone, language, dataset)
  
  out = init_output_request()
  
  out['CHARTS1_TITLE'] = get_text(web_page_content_language, 'Equal', language)
  out['CHARTS1_X_LABEL'] = get_text(web_page_content_language, 'Temperature_label', language)
  out['CHARTS1_Y_LABEL'] = get_text(web_page_content_language, 'Max_Decarbonization_label', language)
  
  out['CHARTS2_TITLE'] = get_text(web_page_content_language, 'Differentiated', language)
  out['CHARTS2_X_LABEL'] = get_text(web_page_content_language, 'Temperature_label', language)
  out['CHARTS2_Y_LABEL'] = get_text(web_page_content_language, 'Max_Decarbonization_label', language)
  
  out['CHARTS3_TITLE'] = get_text(web_page_content_language, 'Equal', language)
  out['CHARTS3_X_LABEL'] = get_text(web_page_content_language, 'YDm', language)
  out['CHARTS3_Y_LABEL'] = get_text(web_page_content_language, 'Max_Decarbonization_label', language)
  
  out['CHARTS4_TITLE'] = get_text(web_page_content_language, 'Differentiated', language)
  out['CHARTS4_X_LABEL'] = get_text(web_page_content_language, 'YDm', language)
  out['CHARTS4_Y_LABEL'] = get_text(web_page_content_language, 'Max_Decarbonization_label', language)
  
  (effort_benchmark_x, effort_benchmark_y) = modelunfair.compute_effort_temperature_benchmark_curve(id_zone)
  out['CHARTS1'][zone + '  :  ' + get_text(web_page_content_language, 'YDm', language) + "="+ str(tmax)] = convert_curve_for_display(effort_benchmark_x, effort_benchmark_y)
  
  (effort_benchmark_x, effort_benchmark_y) = modelfair.compute_effort_temperature_benchmark_curve(id_zone)
  out['CHARTS2'][zone + '  :  ' + get_text(web_page_content_language, 'YDm', language) + "="+ str(tmax)] = convert_curve_for_display(effort_benchmark_x, effort_benchmark_y)
  
  temperature_l = dataset.get_possible_temperature()
  
  for temperature in temperature_l:
    (effort_benchmark_x, effort_benchmark_y) = modelunfair.compute_effort_tmax_benchmark_curve(id_zone, temperature)
    out['CHARTS3'][zone + ' ' + str(temperature) + '°C'] = convert_curve_for_display(effort_benchmark_x, effort_benchmark_y)
    
    (effort_benchmark_x, effort_benchmark_y) = modelfair.compute_effort_tmax_benchmark_curve(id_zone, temperature)
    out['CHARTS4'][zone + ' ' + str(temperature) + '°C'] = convert_curve_for_display(effort_benchmark_x, effort_benchmark_y)
  
  return out


def EXPERT_request(request_data, data_name, language):
  
  tmax = int(request_data['tmax'])
  r = float(request_data['r'])
  gamma = float(request_data['gamma'])
  theta = float(request_data['theta'])
  free_sigma = float(request_data['free_sigma'])
  
  temperature = float(request_data['temperature'])
  
  indc_required = request_data['show_indc']
  
  dataset = redemLib.DataSet(data_name)
  dataset.load_from_sqlite(DATABASE_FILE)
  model = redemLib.Model(dataset)
  model.configure_model(temperature, tmax, r, gamma, theta, free_sigma)
    
  out = init_output_request()
  
  out['CHARTS1_TITLE'] = str(temperature) + '°C'
  out['CHARTS1_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS1_Y_LABEL'] = get_text(web_page_content_language, 'Emissions', language) + ' (' + model.dataset.data['units']['emission'] + ')'
  
  out['CHARTS2_TITLE'] = str(temperature) + '°C'
  out['CHARTS2_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS2_Y_LABEL'] = get_text(web_page_content_language, 'Decarbonization_label', language)

  for zone in request_data.keys():
    id_zone = remove_prefix(zone)
    
    if id_zone and request_data[zone] == "true":
      zone = get_name_of_zone(id_zone, language, dataset)
      
      (emission_x, emission_y) = model.compute_curves(id_zone, temperature)
      (decarbonization_x, decarbonization_y) = model.compute_decarbonization_curve(id_zone)
      out['CHARTS1'][zone] = convert_curve_for_display(emission_x, emission_y)
      out['CHARTS2'][zone] = convert_curve_for_display(decarbonization_x, decarbonization_y)
      
      if indc_required == "true":
        indc = get_INDC(model, id_zone)
        if indc:
          out['CHARTS1'][zone + ' INDC'] = indc
    
  return out


def ENVELOPE_request(request_data, data_name, language):
  
  tmax_min = int(request_data['tmax_min'])
  tmax_max = int(request_data['tmax_max']) + 1
  r_min = int(request_data['r_min'])
  r_max = int(request_data['r_max']) + 1
  gamma_min = int(request_data['gamma_min'])
  gamma_max = int(request_data['gamma_max']) + 1
  theta_min = int(request_data['theta_min'])
  theta_max = int(request_data['theta_max']) + 1
  free_sigma_min = int(request_data['free_sigma_min'])
  free_sigma_max = int(request_data['free_sigma_max']) + 1
  zone = request_data['zone']
  
  temperature = float(request_data['temperature'])
  
  indc_required = request_data['show_indc']
  
  id_zone = remove_prefix(zone)
  
  out = init_output_request()
  
  dataset = redemLib.DataSet(data_name)
  dataset.load_from_sqlite(DATABASE_FILE)
  model = redemLib.Model(dataset)
  model.configure_model(temperature, tmax_min, r_min, gamma_min, theta_min, free_sigma_min)
  
  zone = get_name_of_zone(int(id_zone), language, dataset)
  
  out['CHARTS1_TITLE'] = zone + ': ' + str(temperature) + '°C'
  out['CHARTS1_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS1_Y_LABEL'] = get_text(web_page_content_language, 'Emissions', language) + ' (' + model.dataset.data['units']['emission'] + ')'
  
  out['CHARTS2_TITLE'] = zone + ': ' + str(temperature) + '°C'
  out['CHARTS2_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS2_Y_LABEL'] = get_text(web_page_content_language, 'Decarbonization_label', language)
  
  (emission_x_min, emission_y_min) = model.compute_curves(id_zone, temperature)
  (decarbonization_x_min, decarbonization_y_min) = model.compute_decarbonization_curve(id_zone)
  (emission_x_max, emission_y_max) = model.compute_curves(id_zone, temperature)
  (decarbonization_x_max, decarbonization_y_max) = model.compute_decarbonization_curve(id_zone)

  for r in range(r_min, r_max, 5):
    for gamma in range(gamma_min, gamma_max, 5):
      for theta in range(theta_min, theta_max, 5):
        for free_sigma in range(free_sigma_min, free_sigma_max, 5):
          for tmax in range(tmax_min, tmax_max, 5):
            model.configure_model(temperature, tmax, float(r), float(gamma), float(theta), float(free_sigma))

            (emission_x, emission_y) = model.compute_curves(id_zone, temperature)
            (decarbonization_x, decarbonization_y) = model.compute_decarbonization_curve(id_zone)
            
            for i in range(len(emission_y_min)):
              if emission_y[i] > emission_y_max[i]:
                emission_y_max[i] = emission_y[i]
              if emission_y[i] < emission_y_min[i]:
                emission_y_min[i] = emission_y[i]
            
            for i in range(len(decarbonization_y_min)):
              if decarbonization_y[i] > decarbonization_y_max[i]:
                decarbonization_y_max[i] = decarbonization_y[i]
              if decarbonization_y[i] < decarbonization_y_min[i]:
                decarbonization_y_min[i] = decarbonization_y[i]
  
  emission_data = []
  for x, y_min, y_max in zip(emission_x_min, emission_y_min, emission_y_max):
    emission_data.append([x, y_min, y_max])
  
  decarbonization_data = []
  for x, y_min, y_max in zip(decarbonization_x_min, decarbonization_y_min, decarbonization_y_max):
    decarbonization_data.append([x, y_min, y_max])
  
  out['CHARTS1'][zone] = emission_data
  out['CHARTS2'][zone] = decarbonization_data
  
  if indc_required == "true":
    indc = get_INDC(model, id_zone)
    if indc:
      out['CHARTS1'][zone + ' INDC'] = indc
  
  return out

def RANGE_request(request_data, data_name, language):
  
  min_v = int(request_data['min_v'])
  max_v = int(request_data['max_v']) + 1
  step_v = int(request_data['step_v'])
  option_tmax = int(request_data['option_tmax'])
  option_r = float(request_data['option_r'])
  option_gamma = float(request_data['option_gamma'])
  option_theta = float(request_data['option_theta'])
  option_free_sigma = float(request_data['option_free_sigma'])
  param = request_data['param']
  zone = request_data['zone']
  
  temperature = float(request_data['temperature'])
  
  indc_required = request_data['show_indc']
  
  id_zone = remove_prefix(zone)  
  
  dataset = redemLib.DataSet(data_name)
  dataset.load_from_sqlite(DATABASE_FILE)
  model = redemLib.Model(dataset)
  model.configure_model(temperature, option_tmax, option_r, option_gamma, option_theta, option_free_sigma)
  
  zone = get_name_of_zone(int(id_zone), language, dataset)
  
  out = init_output_request()
  
  out['CHARTS1_TITLE'] = zone + ': ' + str(temperature) + '°C'
  out['CHARTS1_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS1_Y_LABEL'] = get_text(web_page_content_language, 'Emissions', language) + ' (' + model.dataset.data['units']['emission'] + ')'
  
  out['CHARTS2_TITLE'] = zone + ': ' + str(temperature) + '°C'
  out['CHARTS2_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS2_Y_LABEL'] = get_text(web_page_content_language, 'Decarbonization_label', language)
  
  if param == "tmax":
    for tmax in range(min_v, max_v, step_v):
      model.configure_model(temperature, tmax, model.r, model.gamma, model.theta, model.free_sigma)
      (emission_x, emission_y) = model.compute_curves(id_zone, temperature)
      (decarbonization_x, decarbonization_y) = model.compute_decarbonization_curve(id_zone)
      
      out['CHARTS1']["tmax = {0}".format(tmax)] = convert_curve_for_display(emission_x, emission_y)
      out['CHARTS2']["tmax = {0}".format(tmax)] = convert_curve_for_display(decarbonization_x, decarbonization_y)
  elif param == "r":
    for r in range(min_v, max_v, step_v):
      model.configure_model(temperature, model.t_max, float(r), model.gamma, model.theta, model.free_sigma)
      (emission_x, emission_y) = model.compute_curves(id_zone, temperature)
      (decarbonization_x, decarbonization_y) = model.compute_decarbonization_curve(id_zone)

      out['CHARTS1']["r = {0}".format(r)] = convert_curve_for_display(emission_x, emission_y)
      out['CHARTS2']["r = {0}".format(r)] = convert_curve_for_display(decarbonization_x, decarbonization_y)
  elif param == "gamma":
    for gamma in range(min_v, max_v, step_v):
      model.configure_model(temperature, model.t_max, model.r, float(gamma), model.theta, model.free_sigma)
      (emission_x, emission_y) = model.compute_curves(id_zone, temperature)
      (decarbonization_x, decarbonization_y) = model.compute_decarbonization_curve(id_zone)

      out['CHARTS1']["gamma = {0}".format(gamma)] = convert_curve_for_display(emission_x, emission_y)
      out['CHARTS2']["gamma = {0}".format(gamma)] = convert_curve_for_display(decarbonization_x, decarbonization_y)
  elif param == "theta":
    for theta in range(min_v, max_v, step_v):
      model.configure_model(temperature, model.t_max, model.r, model.gamma, float(theta), model.free_sigma)
      (emission_x, emission_y) = model.compute_curves(id_zone, temperature)
      (decarbonization_x, decarbonization_y) = model.compute_decarbonization_curve(id_zone)

      out['CHARTS1']["theta = {0}".format(theta)] = convert_curve_for_display(emission_x, emission_y)
      out['CHARTS2']["theta = {0}".format(theta)] = convert_curve_for_display(decarbonization_x, decarbonization_y)
  elif param == "free_sigma":
    for free_sigma in range(min_v, max_v, step_v):
      model.configure_model(temperature, model.t_max, model.r, model.gamma, model.theta, float(free_sigma))
      (emission_x, emission_y) = model.compute_curves(id_zone, temperature)
      (decarbonization_x, decarbonization_y) = model.compute_decarbonization_curve(id_zone)

      out['CHARTS1']["free_sigma = {0}".format(free_sigma)] = convert_curve_for_display(emission_x, emission_y)
      out['CHARTS2']["free_sigma = {0}".format(free_sigma)] = convert_curve_for_display(decarbonization_x, decarbonization_y)
  
  if indc_required == "true":
    indc = get_INDC(model, id_zone)
    if indc:
      out['CHARTS1'][zone + ' INDC'] = indc
  
  return out


def challenge_answer_insert(answer, title, my_indc_dic, language):
  answer = answer + '<p><strong>' + title + '</strong> :<ul>'
  for year, indc in sorted(my_indc_dic.items(), key=lambda k: k[0]):
    answer = answer + ('<li>%s ' % (get_text(web_page_content_language, 'Since', language))) + str(year) + ' : ' + str(indc) + '%</li>'
  answer = answer + '</ul></p>'
    
  return answer


def CHALLENGE_YOUR_INDC_request(request_data, data_name, language):
  
  tmax = int(request_data['tmax'])
  indc_year = 2030
  zone = request_data['zone']
  
  temperature = float(request_data['temperature'])
  
  id_zone = remove_prefix(zone)
    
  dataset = redemLib.DataSet(data_name)
  dataset.load_from_sqlite(DATABASE_FILE)
  modelunfair = redemLib.Model(dataset)
  modelfair = redemLib.Model(dataset)
  
  modelunfair.configure_model(temperature, tmax, 1.0, 1.0, 1000.0, UNFAIR_SIGMA) #### Unfair World
  modelfair.configure_model(temperature, tmax, 1.0, 1.0, 1000.0, FAIR_SIGMA) #### Fair World
  
  zone = get_name_of_zone(int(id_zone), language, dataset)
  
  out = init_output_request()
  
  out['CHARTS1_TITLE'] = get_text(web_page_content_language, 'Equal', language) + ': ' + str(temperature) + '°C'
  out['CHARTS1_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS1_Y_LABEL'] = get_text(web_page_content_language, 'Emissions', language) + ' (' + modelunfair.dataset.data['units']['emission'] + ')'
  
  out['CHARTS2_TITLE'] = get_text(web_page_content_language, 'Differentiated', language) + ': ' + str(temperature) + '°C'
  out['CHARTS2_X_LABEL'] = get_text(web_page_content_language, 'Year', language)
  out['CHARTS2_Y_LABEL'] = get_text(web_page_content_language, 'Emissions', language) + ' (' + modelunfair.dataset.data['units']['emission'] + ')'
  
  (emission_unfair_x, emission_unfair_y) = modelunfair.compute_curves(id_zone, temperature)
  (emission_fair_x, emission_fair_y) = modelfair.compute_curves(id_zone, temperature)
  
  current_indc = dataset.get_indc_of_zone(id_zone)
  
  emission_index_unfair = emission_unfair_x.index(indc_year)
  optimal_indc_unfair = emission_unfair_y[emission_index_unfair]
  
  emission_index_fair = emission_fair_x.index(indc_year)
  optimal_indc_fair = emission_fair_y[emission_index_fair]

  if current_indc != None:
    current_indc = {
      1990 : str(get_percentage_indc(dataset, id_zone, current_indc[1], 1990, temperature)),
      2005 : str(get_percentage_indc(dataset, id_zone, current_indc[1], 2005, temperature)),
      2010 : str(get_percentage_indc(dataset, id_zone, current_indc[1], 2010, temperature))
    }
  optimal_indc_unfair = {
    1990 : str(get_percentage_indc(dataset, id_zone, optimal_indc_unfair, 1990, temperature)),
    2005 : str(get_percentage_indc(dataset, id_zone, optimal_indc_unfair, 2005, temperature)),
    2010 : str(get_percentage_indc(dataset, id_zone, optimal_indc_unfair, 2010, temperature))
  }
  optimal_indc_fair = {
    1990 : str(get_percentage_indc(dataset, id_zone, optimal_indc_fair, 1990, temperature)),
    2005 : str(get_percentage_indc(dataset, id_zone, optimal_indc_fair, 2005, temperature)),
    2010 : str(get_percentage_indc(dataset, id_zone, optimal_indc_fair, 2010, temperature))
  }
  
  jumbotron_content = '''
    <h2>
      <div class="row">
        <div class="col-md-offset-4 col-md-8">
          %s
        </div>
      </div>
    </h2>
  ''' % (get_text(web_page_content_language, 'Reduction_emission_label', language))
  
  jumbotron_content = jumbotron_content + get_text(web_page_content_language, 'Challenge_INDC_Description_1', language)
    
  if current_indc == None:
    jumbotron_content = jumbotron_content + ('<p><strong>%s</strong> ( %s ) : %s</p>' % (get_text(web_page_content_language, 'COP21_INDC_label', language), zone, get_text(web_page_content_language, 'No_INDC_label', language)))
  else:
    jumbotron_content = challenge_answer_insert(jumbotron_content, get_text(web_page_content_language, 'COP21_INDC_label', language) + ' <SMALL> ( '+ zone + ' ) </SMALL> ' , current_indc, language)

  jumbotron_content = jumbotron_content + get_text(web_page_content_language, 'Challenge_INDC_Description_2', language) 
  
  jumbotron_content = challenge_answer_insert(jumbotron_content, get_text(web_page_content_language, 'Optimal_INDC_Equal_label', language) + ' <SMALL> ( '+ zone + ', ' + str(temperature) + '°C ) </SMALL> ', optimal_indc_unfair, language)
  
  jumbotron_content = challenge_answer_insert(jumbotron_content, get_text(web_page_content_language, 'Optimal_INDC_Differentiated_label', language) + ' <SMALL> ( '+ zone + ', ' + str(temperature) + '°C ) </SMALL> ', optimal_indc_fair, language)
  
  out['CHARTS1'][zone] = convert_curve_for_display(emission_unfair_x, emission_unfair_y)
  out['CHARTS2'][zone] = convert_curve_for_display(emission_fair_x, emission_fair_y)
  
  cop_indc = dataset.get_indc_of_zone(id_zone)
  
  if cop_indc != None:
    out['CHARTS1'][zone + ' INDC'] = convert_curve_for_display([cop_indc[0], cop_indc[0]], [cop_indc[1], cop_indc[2]])
    out['CHARTS2'][zone + ' INDC'] = convert_curve_for_display([cop_indc[0], cop_indc[0]], [cop_indc[1], cop_indc[2]])
  
  out['JUMBOTRON_CONTENT'] = jumbotron_content
  
  return out


def redemCurve(request, language):
  data_name = 'EDGAR'
  
  if request.is_ajax() and request.method == 'POST':
    request_data = request.POST
    app = request_data['APP']
    if app == 'OVERVIEW':
      print('OVERVIEW')
      out = OVERVIEW_request(request_data, data_name, language)
    elif app == 'VISUALIZE':
      print('VISUALIZE')
      out = VISUALIZE_request(request_data, data_name, language)
    elif app == 'EFFORT_VISUALIZATION':
      print('EFFORT_VISUALIZATION')
      out = EFFORT_VISUALIZATION_request(request_data, data_name, language)
    elif app == 'EXPERT':
      print('EXPERT')
      out = EXPERT_request(request_data, data_name, language)
    elif app == 'ENVELOPE':
      print('ENVELOPE')
      out = ENVELOPE_request(request_data, data_name, language)
    elif app == 'RANGE':
      print('RANGE')
      out = RANGE_request(request_data, data_name, language)
    elif app == 'CHALLENGE_YOUR_INDC':
      print('CHALLENGE_YOUR_INDC')
      out = CHALLENGE_YOUR_INDC_request(request_data, data_name, language)
    else:
      out = {}
    
    out = json.dumps(out, sort_keys=True, indent=2, separators=(',', ': '))

    return HttpResponse(out, content_type='application/json')
  else:
    web_page_content = get_web_page_content(web_page_content_language, language)
    
    dataset = redemLib.DataSet(data_name)
    dataset.load_from_sqlite(DATABASE_FILE)
        
    all_country = str(dataset.get_id_world())
    all_country_name = get_name_of_zone(dataset.get_id_world(), language, dataset)
    region_list = dataset.get_region_list()
  
    region_assos = []
  
    for region in region_list:
      country_list = dataset.get_country_of_region(region)
      country_with_detail = []
      
      for country in country_list:
        country_with_detail.append((str(country), dataset.get_name_of_zone(country, language)))
      
      region_assos.append((str(region), dataset.get_name_of_zone(region, language), sorted(country_with_detail, key=lambda c: c[1])))
    
    region_assos = sorted(region_assos, key=lambda k: k[1])
    
    temperature_l = dataset.get_possible_temperature()
    language_l = dataset.get_possible_languages()
    
    return render(request, 'redemCurve/redemCurve.html', locals())

