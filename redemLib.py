#### Redem-lib Project
import os
import math
import numpy
import matplotlib.pyplot as plt
import sqlite3
from scipy.optimize import newton, root
from scipy.integrate import quad
from scipy.spatial import distance
import os
from django.conf import settings



CRI_YEAR = 2010

################
class SQLite_Climate_Interface:
  """Implementation of SQLite interface for REDEM climate database."""
  
  def __init__(self, sqlite_file, data_name):
    self.sqlite_file = sqlite_file
    self.data_name = data_name
    
    query_answers = self.select_query(
      """SELECT id_country, id_region, id_emission_co2, id_emission_ghg, id_LULUCF, id_gdp, id_population, id_INDC, id_budget, id
         FROM DataSet
         WHERE name=:data_name """,
      {'data_name' : data_name}
    )
    
    query_answer = query_answers[0]
    self.id_country = query_answer[0]
    self.id_region = query_answer[1]
    self.id_emission_co2 = query_answer[2]
    self.id_emission_ghg = query_answer[3]
    self.id_LULUCF = query_answer[4]
    self.id_gdp = query_answer[5]
    self.id_population = query_answer[6]
    self.id_INDC = query_answer[7]
    self.id_budget = query_answer[8]
    self.id = query_answer[9]
  
  
  @staticmethod
  def list_dataset_of_file(file):
    """List the name of all the DataSet present in the database file.
    
    cf. the representation of a climate Database for more informations about a DataSet
    """
    conn = sqlite3.connect(file)
    cursor = conn.cursor()
    
    cursor.execute(
    """SELECT name, author, comment
       FROM DataSet""", {}
    )
    out = cursor.fetchall()
    conn.close()
    
    return list(out)
  
  def select_query(self, query, query_data):
    """Execute a Select query with some specified query_data.
    
    It opens and closes the database file properly.
    """
    # Different path in dev or prod mode
    # a tester sur serveur apache: print('RPG VARLOG ', settings.BASE_DIR)
    try:
      filename = os.environ['REDEM_DB_PATH'] + '/' + self.sqlite_file
      conn = sqlite3.connect(filename)
    except:
      conn = sqlite3.connect(self.sqlite_file)

    cursor = conn.cursor()
    
    cursor.execute(query, query_data)
    out = cursor.fetchall()
    conn.close()
    
    return out
  
  
  @staticmethod
  def tuple_of_tuple_to_list(tuple_tuple):
    """Transform a tuple of single tuple to a list.
    
    ((a,), (b,), (c,)) ---> [a, b, c]
    """
    out = []
    for tuple_elem in tuple_tuple:
      out.append(tuple_elem[0])
    
    return out

  
  def load_budget(self):
    """Load from the database every budget.
    
    The output is a dictionnary with the following fields:
      -'budget' : a dictionnary witch associated a temperature (float) to a carbon budget (float)
      -'t_0' : the year where all the budget begin. This year is included
      -'t_final' : the year where all the budget end. This year is included
    
    """
    query_answers = self.select_query(
      """SELECT temperature, main_budget, year_ini, year_final
         FROM Budget
         WHERE id = :id_budget""",
      {'id_budget' : self.id_budget}
    )
    
    out = {
      'budget' : {},
      't_0' : -1,
      't_final' : -1,
    }
    
    for budget_tuple in query_answers:
      temperature = budget_tuple[0]
      main_budget = budget_tuple[1]
      year_ini = budget_tuple[2]
      year_final = budget_tuple[3]
      
      out['budget'][temperature] = main_budget
      out['t_0'] = year_ini
      out['t_final'] = year_final
    
    return out
  
  
  def load_countries(self):
    """Load from the database every country codes.
    
    The output is a list of all the country codes.
    """
    query_answers = self.select_query(
      """SELECT id_country
         FROM Country
         WHERE id=:id""",
      {'id' : self.id_country}
    )
    
    return SQLite_Climate_Interface.tuple_of_tuple_to_list(query_answers)
  
  
  def load_countries_name(self):
    """Load from the database every country names.
    
    The output is a dictionnary with the following fields:
      -for each language, there is a field which contains a dictionnary:
        -for each country code, there is a field which contains the name of the
         country in the right language
    """
    query_answers = self.select_query(
      """SELECT id_country, language, name
         FROM Name_country
         WHERE id_country_table=:id""",
      {'id' : self.id_country}
    )
    
    out = {}
    
    for country_name_tuple in query_answers:
      id_country = country_name_tuple[0]
      language = country_name_tuple[1]
      name = country_name_tuple[2]
      
      if language not in out.keys():
        out[language] = {}
      out[language][id_country] = name
    
    return out
  
  
  def load_regions(self):
    """Load from the database every regions.
    
    The output is a dictionnary with the following fields:
      -for each region code, there is a field which contains the list of the
       country code which are present in the region
    """
    query_answers = self.select_query(
      "SELECT DISTINCT id_region " +
      "FROM Region " +
      "WHERE id = :id_region",
      {'id_region' : self.id_region}
    )
    
    out = {}
    
    region_list = SQLite_Climate_Interface.tuple_of_tuple_to_list(query_answers)
    
    for region in region_list:
      query_answers = self.select_query(
        "SELECT id_country " +
        "FROM Region " +
        "WHERE id=:id_region_table AND id_region=:id_region",
        {'id_region_table' : self.id_region, 'id_region' : region}
      )
      country_list = SQLite_Climate_Interface.tuple_of_tuple_to_list(query_answers)
      out[region] = country_list
    
    return out
  
  
  def load_emission_co2(self, year):
    """Load from the database all the CO2 emissions.
    
    The output is a dictionnary with the following fields:
      -for each country code, there is a field which countains a dictionnary:
        -for each date of emissions of the country code, there is a field which
         contains the emissions of the CO2 of this year (units are provided in the database)
    """
    query_answers = self.select_query(
      """SELECT id_country, year, emission
         FROM Emission_CO2
         WHERE id=:id_emission_co2 AND year<=:year""",
      {'id_emission_co2' : self.id_emission_co2, 'year' : year}
    )
    
    out = {}
    
    for emission_co2_tuple in query_answers:
      id_country = emission_co2_tuple[0]
      emission_year = emission_co2_tuple[1]
      emission = emission_co2_tuple[2]
      
      country_emissons = out.get(id_country, {})
      country_emissons[emission_year] = emission
      out[id_country] = country_emissons
    
    return out
  
  
  def load_emission_ghg(self):
    """Load from the database all the emissions of other GHG (CH4, N2O).
    
    The emissions of other GHG are based on different scenarios which respectec
    different temperature elevation
    
    The output is a dictionnary with the following fields:
      -for each temperature, there is a field which contains a dictionnary:
        -for each country code, there is a field which contains a dictionnary:
          -for each date of emissions of the country code for the temperature,
           there is a field which contains the emissions of the others GHG of
           this year (units are provided in the database)
    """
    query_answers = self.select_query(
      """SELECT temperature, id_country, year, emission
         FROM Emission_Other_GHG
         WHERE id=:id_emission_ghg""",
      {'id_emission_ghg' : self.id_emission_ghg}
    )
    
    out = {}
    
    for emission_ghg_tuple in query_answers:
      temperature = emission_ghg_tuple[0]
      id_country = emission_ghg_tuple[1]
      emission_year = emission_ghg_tuple[2]
      emission = emission_ghg_tuple[3]
      
      temperature_country_emission = out.get(temperature, {})
      country_emissons = temperature_country_emission.get(id_country, {})
      country_emissons[emission_year] = emission
      temperature_country_emission[id_country] = country_emissons
      out[temperature] = temperature_country_emission
    
    return out
  
  
  def load_lulucf(self, year):
    """Load from the database all the LULUCF emissions.
    
    The output is a dictionnary with the following fields:
      -for each country code, there is a field which countains a dictionnary:
        -for each date of emissions of the country code, there is a field which
         contains the LULUCF emissions of this year (units are provided in the database)
    """
    query_answers = self.select_query(
      """SELECT id_country, year, emission
         FROM LULUCF
         WHERE id=:id_lulucf AND year<=:year""",
      {'id_lulucf' : self.id_LULUCF, 'year' : year}
    )
    
    out = {}
    
    for lulucf_tuple in query_answers:
      id_country = lulucf_tuple[0]
      emission_year = lulucf_tuple[1]
      emission = lulucf_tuple[2]
      
      country_lulucf = out.get(id_country, {})
      country_lulucf[emission_year] = emission
      out[id_country] = country_lulucf
    
    return out
  
  
  def load_gdp(self, year):
    """Load from the database the GDP of a given year (Ground Domestic Product).
    
    The output is a dictionnary with the following fields:
      -for each country code, there is a field which contains
       his gdp (units are provided in the database)
    """
    query_answers = self.select_query(
      """SELECT id_country, gdp
         FROM GDP
         WHERE id=:id_gdp AND year=:year""",
      {'id_gdp' : self.id_gdp, 'year' : year}
    )
    
    out = {}
    
    for gdp_tuple in query_answers:
      country = gdp_tuple[0]
      gdp = gdp_tuple[1]
      out[country] = gdp
    
    return out
  
  
  def load_population(self, year):
    """Load from the database the population of a given year.
    
    The output is a dictionnary with the following fields:
      -for each country code, there is a field which contains
       his population (units are provided in the database)
    """
    query_answers = self.select_query(
      "SELECT id_country, population " +
      "FROM Population " +
      "WHERE id=:id_population AND year=:year",
      {'id_population' : self.id_population, 'year' : year}
    )
    
    out = {}
    
    for population_tuple in query_answers:
      country = population_tuple[0]
      population = population_tuple[1]
      out[country] = population
    
    return out
  
  
  def load_indc(self):
    """Load from the database all the INDC.
    
    The output is a dictionnary with the following fields:
      -for each zone code (country or region) which has an INDC,
       there is a field which is a tuple:
         ---> (year of INDC, INDC MIN, INDC MAX)
    """
    query_answers = self.select_query(
      "SELECT id_area, year, indc_min, indc_max " +
      "FROM INDC " +
      "WHERE id=:id_indc",
      {'id_indc' : self.id_INDC}
    )
    
    out = {}
    
    for indc_tuple in query_answers:
      id_area = indc_tuple[0]
      year = indc_tuple[1]
      indc_min = indc_tuple[2]
      indc_max = indc_tuple[3]
      out[id_area] = (year, indc_min, indc_max)
    
    return out
  
  
  def load_units(self):
    """Load from the database the units.
    
    The output is a dictionnary with the following fields:
      -'emission' : a string describing the unit of all the emissions presents in the database
      -'gdp' : a string describing the unit of all the GDP presents in the database
      -'population' : a string describing the unit of all the population presents in the database
    """
    query_answers = self.select_query(
      "SELECT units_emission, units_gdp, units_population " +
      "FROM DataSet " +
      "WHERE name=:data_name",
      {'data_name' : self.data_name}
    )
    
    query_answer = query_answers[0]
    
    return {
      'emission' : query_answer[0],
      'gdp' : query_answer[1],
      'population' : query_answer[2],
    }
  
  
  def load_curves(self):
    """Load from the database all the curves (precomputed curves which are stored in the database).
    
    The output is a dictionnary with the following fields:
      -for each curve id, there is a field which is a tuple:
        ---> (List of X, List of Y)
    """
    query_answers = self.select_query(
      """SELECT DISTINCT id
         FROM CURVE""",
      {}
    )
    
    id_list = SQLite_Climate_Interface.tuple_of_tuple_to_list(query_answers)
    
    out = {}
    for id_curve in id_list:
      query_answers = self.select_query(
        """SELECT x, y
           FROM CURVE
           WHERE id=:id""",
        {'id' : id_curve}
      )
      new_curves = []
      
      for curve_tuple in query_answers:
        x = curve_tuple[0]
        y = curve_tuple[1]
        new_curves.append((x, y))
      
      new_curves = sorted(new_curves, key=lambda k: k[0])
      
      new_x = []
      new_y = []
      for (x, y) in new_curves:
        new_x.append(x)
        new_y.append(y)
      
      out[id_curve] = (new_x, new_y)
    
    return out
  
  
  def load_parameters(self):
    """Load from the database all the parameters (precomputed set of parameters which are stored in the database).
    
    The output is a list of tuple:
      ---> (Mu bar, (Temperature, tmax, r, gamma, theta, sigma bar))
    cf. the paper of Emmanuel Prados for details about those parameters
    """
    query_answers = self.select_query(
      """SELECT DISTINCT free_mu, temperature, year_max, r, gamma, theta, free_sigma
         FROM PARAMETERS
         WHERE id_data=:id""",
      {'id' : self.id}
    )
    
    out = []
    
    for parameter_tuple in query_answers:
      free_mu = parameter_tuple[0]
      temperature = parameter_tuple[1]
      year_max = parameter_tuple[2]
      r = parameter_tuple[3]
      gamma = parameter_tuple[4]
      theta = parameter_tuple[5]
      free_sigma = parameter_tuple[6]
      
      out.append((free_mu, (temperature, year_max, r, gamma, theta, free_sigma)))
    
    return out

################
class DataSet:
  """Object which should store the different climate data."""
  
  def __init__(self, data_name):
    self.all_countries_id = 'World'
    self.data = {
      'name' : data_name,
      'budget' : {},
      't_0' : -1,
      't_final' : -1,
      'countries' : [],
      'country_name' : {},
      'regions' : {},
      'emission_co2' : {},
      'emission_ghg' : {},
      'lulucf' : {},
      'gdp' : {},
      'population' : {},
      'indc' : {},
      'units' : {
        'emission' : '',
        'gdp' : '',
        'population' : '',
      },
      'parameters' : []
    }
  
  
  def get_t_0(self):
    """Return the year where all the budget begin (the year is included)"""
    return self.data['t_0']
  
  
  def get_t_final(self):
    """Return the year where all the budget end (the year is included)"""
    return self.data['t_final']
  
  
  def get_id_world(self):
    """Return the region id of the world.
    
    The world is considered as a region but is not the data. It is simply a region
    composed of all the countries. It has a special id.
    """
    return -1
  
  
  def get_budget_of_temperature(self, temperature):
    """Return the budget for the given temperature."""
    return self.data['budget'][temperature]
  
  
  def get_country_of_region(self, region):
    """Return the list of country contained in the given region."""
    if region == self.get_id_world():
      return self.get_country_list()
    else:
      return self.data['regions'][region]
  
  
  def get_region_list(self):
    """Return the list of all the region present in the data."""
    out = list(self.data['regions'].keys())
    
    return out
  
  
  def get_country_list(self):
    """Return the list of all the countries in the data."""
    out = []
    for zone in self.data['countries']:
      if not self.is_region(zone):
        out.append(zone)
    
    return out
  
  
  def is_region(self, zone):
    """Return a boolean telling if the zone provided is region or not."""
    return zone in self.data['regions'].keys() or zone == self.get_id_world()
  
  
  def get_name_of_zone(self, zone, language):
    """Return the name of the zone with the appropiate language."""
    return self.data['country_name'][language][zone]
  
  
  def get_id_of_zone(self, name, language):
    """Return the id of a zone witch has the name name in the language language."""
    for id_zone, zone in self.data['country_name'][language].items():
      if zone == name:
        return id_zone
    
    return None
  
  
  def get_indc_of_zone(self, zone):
    return self.data['indc'].get(zone)
  
  
  def get_possible_temperature(self):
    """Return all the temperature which are present in the data.
    
    The temperature returned are the temperature binded with a budget in the data.
    """
    return sorted(list(self.data['budget'].keys()))
  
  
  def get_possible_languages(self):
    return sorted(list(self.data['country_name'].keys()))
  
  
  def get_complete_co2_emissions(self, id_country, year):
    """ Return the complete CO2 emissions of one year.
    
    It means the CO2 emissions plus the LULUCF emissions.
    """
    emission_co2 = self.data['emission_co2'][id_country][year]
    emission_lulucf = self.data['lulucf'][id_country][year]
    
    return emission_co2 + emission_lulucf
  
  
  def get_complete_co2_emissions_list(self, id_country, year_max):
    """Return the complete CO2 emissions of a zone.
    
    The emissions begin with the smallest year present in the data and end with the year_max.
    """
    emission_x = list(self.data['emission_co2'][id_country].keys())
    lulucf_x = list(self.data['lulucf'][id_country].keys())
    
    year_min = max(min(emission_x), min(lulucf_x))
    
    out_x = list(range(year_min, year_max))
    out_y = []
    for year in out_x:
      emission = self.get_complete_co2_emissions(id_country, year)
      out_y.append(emission)
    
    return (out_x, out_y)
  
  
  def get_ghg_emissions(self, temperature, id_country):
    """Return the emissions of other GHG of a zone.
    
    It return the emissions for all the years that are presents in the data.
    """
    out_x = []
    out_y = []
    
    for x, y in sorted(self.data['emission_ghg'][temperature][id_country].items(), key=lambda ghg: ghg[0]):
      out_x.append(x)
      out_y.append(y)
    
    return (out_x, out_y)
  
  
  def get_emissions_of_zone(self, id_zone, year, temperature):
    """Return the complete emissions of GHG of a zone for the year year.
    
    If the data for the other GHG is not present in the data, it is assumed to be zero.
    """
    if self.is_region(id_zone):
      country_list = self.get_country_of_region(id_zone)
      out = 0
      for country in country_list:
        out = out + self.get_emissions_of_zone(country, year, temperature)
      
      return out
    else:
      co2_emisions = self.get_complete_co2_emissions(id_zone, year)
      ghg_emissions = self.data['emission_ghg'][temperature][id_zone].get(year, 0)
            
      return co2_emisions + ghg_emissions
  
  
  def get_gdp(self, id_country):
    return self.data['gdp'][id_country]
  
  
  def get_population(self, id_country):
    return self.data['population'][id_country]
  
  
  def get_free_mu_close_to(self, temperature, tmax, r, gamma, theta, free_sigma):
    """Return a value for mu bar which correspond to a set of parameters in the data which 
    is the closest to the data provided.
    
    The distance between the parameters provided and the parameters in tthe data is a simple 
    euclidian distance.
    """
    dist_parameters_l = []
    
    for free_mu, parameters_vector in self.data['parameters']:
      dst = distance.euclidean(parameters_vector, (temperature, tmax, r, gamma, theta, free_sigma))
      dist_parameters_l.append((free_mu, parameters_vector, dst))
    
    try:
      best_choice = min(dist_parameters_l, key=lambda k: k[2]) # Minimisation de la distance
      return best_choice[0]
    except ValueError:
      return 0.0
    
  
  def load_from_sqlite(self, sqlite_file):
    """Load data from a SQLite database containing the REDEM climate data."""
    
    sqlite_interface = SQLite_Climate_Interface(sqlite_file, self.data['name'])
    
    budget_detail = sqlite_interface.load_budget()
        
    self.data['budget'] = budget_detail['budget']
    self.data['t_0'] = budget_detail['t_0']
    self.data['t_final'] = budget_detail['t_final']
    self.data['countries'] = sqlite_interface.load_countries()
    self.data['country_name'] = sqlite_interface.load_countries_name()
    self.data['regions'] = sqlite_interface.load_regions()
    self.data['emission_co2'] = sqlite_interface.load_emission_co2(self.data['t_0'])
    self.data['emission_ghg'] = sqlite_interface.load_emission_ghg()
    self.data['lulucf'] = sqlite_interface.load_lulucf(self.data['t_0'])
    self.data['gdp'] = sqlite_interface.load_gdp(CRI_YEAR)
    self.data['population'] = sqlite_interface.load_population(CRI_YEAR)
    self.data['indc'] = sqlite_interface.load_indc()
    self.data['units'] = sqlite_interface.load_units()
    self.data['parameters'] = sqlite_interface.load_parameters()


################
class Model:
  def __init__(self, dataset):
    """Create an object Model.
    
    dataset is a loaded DataSet object.
        
    """
    self.dataset = dataset
    
    self.t_0 = self.dataset.get_t_0()
    self.t_final = self.dataset.get_t_final()
    self.carbon_budget = 0
    
    self.temperature = 0
    self.t_max = 0
    self.r = 0
    self.gamma = 0
    self.theta = 0
    self.free_sigma = 0
    
    self.capacity_responsility_index = {}
    self.initial_effort = {}
    self.mu = 0.0
    self.sigma = 0.0
    self.free_mu = 0.0
  
  
  def init_param_r(self, r):
    """Initialisation of the r paramater which reprensent the balance between GDP and emission in the CRI.
    
    The CRI, his average and his standard deviation are computed again with the new value of r.
    
    """
    self.r = r
    self.capacity_responsility_index = self.compute_cri()
        
    tmp = numpy.array(list(self.capacity_responsility_index.values()))
    self.mu = numpy.mean(tmp)
    self.sigma = numpy.std(tmp)
    
    if self.sigma == 0:
      self.sigma = 1
    
  
  def configure_model(self, temperature, year_max, r, gamma, theta, free_sigma):
    """Configure a model with his parameters.
    
    year_max is the tmax of the model, the year where the effort should be maximal.
    r is the balance between GDP and emission in the CRI.
    gamma and theta are accelerations terms for respectively the first phase and the second phase.
    free_sigma is coefficient witch gives more or less importance to the CRI.
    
    """
    self.temperature = temperature
    self.t_max = year_max
    self.gamma = gamma
    self.theta = theta
    self.free_sigma = free_sigma
    
    self.carbon_budget = self.dataset.get_budget_of_temperature(temperature)
    
    self.initial_effort = self.compute_Di()
    self.init_param_r(r)
    
    self.compute_free_mu()


  @staticmethod
  def integrate(f, a, b, step):
    """Compute the integral of the function f between a and b using the rectangle rule with the step step
    
    The boundary b is included in the computation.
    """
    out = 0
    for i in range(a, b+1, step):
      out += f(i) * step
    return out
  
  
  @staticmethod
  def effort_rate(val, val_prev):
    """Compute the discrite logarithmic derivative of the given values."""
    return -100 * (val - val_prev) / val
  
  
  def compute_Ei(self, t, free_mu, t_max, gamma, theta, free_sigma, country):
    """Compute the emissions of the country country in the year t using the REDEM algorithm.
    free_mu is a parameter of the REDEM algorithm.
    
    """
    out = 0
    ni_tilde = free_mu + (free_sigma / self.sigma) * (self.capacity_responsility_index[country] - self.mu)
    ai = (self.initial_effort[country] - ni_tilde) / ((t_max - self.t_0)**gamma)
    ci = (self.initial_effort[country] - ni_tilde) * (t_max - self.t_0) / (gamma + 1) - ni_tilde * self.t_0
    if t < t_max:
      out = ai * ((t_max - t)**(gamma + 1)) / (gamma + 1) - ni_tilde * t - ci
      out = math.exp(out / 100) * self.dataset.get_complete_co2_emissions(country, self.t_0)
    else:
      out = ni_tilde * t_max + ci
      out += theta * math.sqrt(math.pi/2) * ni_tilde * math.erf((t - t_max)/(math.sqrt(2) * theta))
      out = math.exp(-1 * out / 100) * self.dataset.get_complete_co2_emissions(country, self.t_0)
    
    return out
    
  
  def free_mu_optimization(self, gamma, free_mu):
    """f is a function witch is meant to be given to the Newton algorithm in order to find its zero.
    It should not be used directly.
    
    """
    out = 0
    for country in self.dataset.get_country_list():
      out += Model.integrate(
        lambda t: self.compute_Ei(t, free_mu, self.t_max, gamma, self.theta, self.free_sigma, country),
        self.t_0, self.t_final, 1
      )
    out -= self.carbon_budget
    return out
  
  
  def compute_first_delta_Ei(self, t, gamma, free_mu, country):
    """Computation of the first part of the derivative of f for t in [year, year_max].
    It should not be used directly.
    
    """
    out = -1.0 * (self.t_max - t)**(gamma + 1)
    out /= (gamma + 1) * (self.t_max - self.t_0)**gamma
    out -= t
    out += (self.t_max + gamma * self.t_0) / (gamma + 1)
    out *= self.compute_Ei(t, free_mu, self.t_max, gamma, self.theta, self.free_sigma, country) / 100.0
    return out
  
  
  def compute_second_delta_Ei(self, t, gamma, free_mu, country):
    """Computation of the second part of the derivative of f for t in [year_max, year_end].
    It should not be used directly.
    
    """
    out = self.t_max
    out -= (self.t_max + gamma * self.t_0) / (gamma + 1)
    out += self.theta * math.sqrt(math.pi / 2) * math.erf((t - self.t_max) / (math.sqrt(2) * self.theta))
    out *= -1 * self.compute_Ei(t, free_mu, self.t_max, gamma, self.theta, self.free_sigma, country) / 100
    return out
    
  
  def free_mu_optimization_prime(self, gamma, free_mu):
    """Computation of the derivative of f for the Newton algorithm.
    It should not be used directly.
    
    """
    out = 0
    for country in self.dataset.get_country_list():
      val_1 = Model.integrate(lambda t: self.compute_first_delta_Ei(t, gamma, free_mu, country), self.t_0, self.t_max, 1)
      val_2 = Model.integrate(lambda t: self.compute_second_delta_Ei(t, gamma, free_mu, country), self.t_max, self.t_final, 1)
      out += val_1 + val_2
    return out
  
  
  def compute_free_mu(self):
    """Computation of the free parameters free_mu in order to force the emissions to respect the given budget.
    
    It uses a Newton method to find the zero of the function f.
    
    """
    initial_guess = self.dataset.get_free_mu_close_to(
      self.temperature, self.t_max, self.r,
      self.gamma, self.theta, self.free_sigma
    )
        
    self.free_mu = newton(lambda free_mu: self.free_mu_optimization(self.gamma, free_mu), initial_guess,
                          fprime = lambda free_mu: self.free_mu_optimization_prime(self.gamma, free_mu))
  
  
  def compute_cri(self):
    """Computation of the CRI (Capacity Responsability Index) of every country of our model."""
    out = {}
    for country in self.dataset.get_country_list():
      gdp_kilo_dollars = self.dataset.get_gdp(country) / 1000000000
      emission_ton = self.dataset.get_complete_co2_emissions(country, CRI_YEAR) / 1000
      population_millier = self.dataset.get_population(country) / 1000000
      
      gdp_per_capita = gdp_kilo_dollars / population_millier
      emission_per_capita = emission_ton / population_millier
      
      out[country] = (self.r * gdp_per_capita + emission_per_capita)

    delta = max(out.values())
    for country in out.keys():
      out[country] = out[country] / delta
        
    return out

  
  def compute_Di(self):
    """Computation of the initial effort for every country of our model."""
    out = {}
    for country in self.dataset.get_country_list():
      emissions = self.dataset.get_complete_co2_emissions(country, self.t_0)
      emissions_prev = self.dataset.get_complete_co2_emissions(country, self.t_0 - 1)
      di = Model.effort_rate(emissions, emissions_prev)
      if di < -5:
        di = -5
      if di > 5:
        di = 5
      out[country] = di
    return out
  
  
  def compute_curves_country_co2(self, country):
    """Compute the emission curve of the given country.
    
    The emission curve begins with the earliest emission data that is provided by our data set.
    
    This function should not be used directly.
    """
    emission_x = list(range(self.t_0, self.t_final+1))
    emission_y = []
    for t in emission_x:
      emission_y.append(self.compute_Ei(t, self.free_mu, self.t_max, self.gamma, self.theta, self.free_sigma, country))
    
    (prev_emission_x, prev_emission_y) = self.dataset.get_complete_co2_emissions_list(country, self.t_0)
    
    emission_x = prev_emission_x + emission_x
    emission_y = prev_emission_y + emission_y
        
    return (emission_x, emission_y)
  
  
  def compute_curves_region_co2(self, country_list):
    """Compute the emission curve of a region.
    
    The emission curve of the region is computed by simply adding the emissions curves 
    of all his component.
    
    The emission curve begins with the earliest emission data that is provided by our data set.
    
    This function should not be used directly.
    """
    
    tmp = []
    curve_x = []
    curve_y = []
    for country in country_list:
      (emission_x, emission_y) = self.compute_curves_country_co2(country)
      tmp.append(emission_x[0])
      curve_x.append(emission_x)
      curve_y.append(emission_y)
    
    valid_begin_year = max(tmp)
    
    emission_x = list(range(valid_begin_year, self.t_final + 1))
    emission_y = [0] * (len(emission_x))
    for (country_x, country_y) in zip(curve_x, curve_y):
      ind = country_x.index(valid_begin_year)
      country_y = country_y[ind:]
      for i in range(len(country_y)):
        emission_y[i] += country_y[i]
        
    return (emission_x, emission_y)
      
  
  def compute_curves_co2(self, zone):
    """Compute the emissions curve of CO2 of the given zone.
    
    The curve begins at the earliest date of emissions that is provided in the data.
    The zone could be a region or a country.
    
    The output is a tuple of two list :
      - the first list is the list of the x values : the years
      - the second list is the list of the y values : the emissions of CO2 corresponding to the years.
    """
    if self.dataset.is_region(zone):
      country_list = self.dataset.get_country_of_region(zone)
      return self.compute_curves_region_co2(country_list)
    else:
      return self.compute_curves_country_co2(zone)
  
  
  def compute_curves_ghg(self, zone, temperature):
    """Compute the emissions curve of other GHG of the given zone.
    
    There is no actual computation, the curve are just taken from the date set.
    For a region, they are agglomerate.
    
    The output is a tuple of two list :
      - the first list is the list of the x values : the years
      - the second list is the list of the y values : the emissions of other GHG corresponding to the years.
    """
    if self.dataset.is_region(zone):
      country_list = self.dataset.get_country_of_region(zone)
    else:
      country_list = [zone]
    
    ghg_curve_x = []
    ghg_curves_y = []
    for country in country_list:
      (ghg_curve_x, ghg_curve_y) = self.dataset.get_ghg_emissions(temperature, country)
      ghg_curves_y.append(ghg_curve_y)
    
    ghg_curve_y = []
    
    for i in range(len(ghg_curves_y[0])):
      elem = 0
      for j in range(len(ghg_curves_y)):
        elem = elem + ghg_curves_y[j][i]
      ghg_curve_y.append(elem)
    
    return (ghg_curve_x, ghg_curve_y)
  
  
  def merge_curve(self, x1, y1, x2, y2):
    """Merge the curves (x1, y1) and (x2, y2).
    
    By merging, we mean adding the two curves in their overlapping domain.
    
    The output is a new curve which is only the sum of those two curves on the ovelapping domain.
    """
    min1 = min(x1)
    min2 = min(x2)
    min_x = max([min1, min2])
    
    max1 = max(x1)
    max2 = max(x2)
    max_x = min([max1, max2])
    
    ind_begin1 = x1.index(min_x)
    ind_end1 = x1.index(max_x)
    
    new_x1 = x1[ind_begin1:ind_end1+1]
    new_y1 = y1[ind_begin1:ind_end1+1]
    
    ind_begin2 = x2.index(min_x)
    ind_end2 = x2.index(max_x)
    
    new_x2 = x2[ind_begin2:ind_end2+1]
    new_y2 = y2[ind_begin2:ind_end2+1]
        
    out_x = new_x1
    out_y = []
    for elem_y1, elem_y2 in zip(new_y1, new_y2):
      out_y.append(elem_y1 + elem_y2)
    
    return (out_x, out_y)
  
  
  def compute_curves(self, zone, temperature):
    """Compute the complete emissions curve of all the GHG of the given zone.
    
    The output is a tuple of two list :
      - the first list is the list of the x values : the years
      - the second list is the list of the y values : the emissions of all the GHG corresponding to the years.
    """
    (emission_x, emission_y) = self.compute_curves_co2(zone)
    (ghg_x, ghg_y) = self.compute_curves_ghg(zone, temperature)
        
    return self.merge_curve(emission_x, emission_y, ghg_x, ghg_y)
  
  
  def compute_decarbonization_curve(self, zone):
    """Compute the decarbonization curve of the given zone.
    
    The decarbonization curve is comuted only on the CO2 emissions curves.
    
    The output is a tuple of two list :
      - the first list is the list of the x values : the years
      - the second list is the list of the y values : the decarbonization rate of the years.
    """
    (emission_x, emission_y) = self.compute_curves_co2(zone)
    decarbonization_index = emission_x.index(self.t_0 + 1)
    
    decarbonization_x = []
    decarbonization_y = []
    
    for i in range(decarbonization_index, len(emission_x)):
      decarbonization_x.append(emission_x[i])
      decarbonization_y.append(Model.effort_rate(emission_y[i], emission_y[i-1]))
    
    return (decarbonization_x, decarbonization_y)

  
  def compute_indc_benchmark_curve(self, zone_list, my_indc=None):
    """Compute the benchmark curves of the INDC of each country zone in the zone_list.
    
    The benchmark curve of the INDC of a zone is a curve representing the gap between the INDC 
    and the emissions which are given by REDEM.
    
    The benchmark is computed with all the temperature that available in our data set of budget.
    
    The output is a tuple :
      - the first list is the list of the x values : the years
      - the second list is a dictionnary which binds the zone to their list of y values : the indc benchmark curves
    """
    out = {}
    indc_benchmark_x = self.dataset.get_possible_temperature()
    
    for temperature in indc_benchmark_x:
      self.configure_model(temperature, self.t_max, self.r, self.gamma, self.theta, self.free_sigma)
      
      for zone in zone_list:
        if my_indc == None:
          (year, indc_min, indc_max) = self.dataset.get_indc_of_zone(zone)
        else:
          (year, indc_min, indc_max) = my_indc
        
        (emission_x, emission_y) = self.compute_curves(zone, temperature)
        
        ind_indc = emission_x.index(year)
        
        (new_y_min, new_y_max) = out.get(zone, ([], []))
        
        new_y_min.append((indc_min - emission_y[ind_indc]))
        new_y_max.append((indc_max - emission_y[ind_indc]))
        
        out[zone] = (new_y_min, new_y_max)

    return (indc_benchmark_x, out)
  
  
  def compute_effort_temperature_benchmark_curve(self, zone):
    """Compute the benchmark curve of the effort of an area over the temperature.
    
    The benchmark curve of the temperature is a curve representing the maximal decarbonisation rate 
    of the zone for each temperature.
    
    The benchmark is computed with all the temperature that available in our data set of budget.
    
    The output is a tuple of two list :
      - the first list is the list of the x values : the temperatures
      - the second list is the list of the y values : the maximal dacrbonisation rate of the corresponding temperatures.
    """
    effort_benchmark_x = self.dataset.get_possible_temperature()
    effort_benchmark_y = []
    
    for temperature in effort_benchmark_x:
      self.configure_model(temperature, self.t_max, self.r, self.gamma, self.theta, self.free_sigma)
      (emission_x, emission_y) = self.compute_curves(zone, temperature)
      (decarbonization_x, decarbonization_y) = self.compute_decarbonization_curve(zone)
      
      index_tmax = decarbonization_x.index(self.t_max)
      
      effort_benchmark_y.append(decarbonization_y[index_tmax])
    
    return (effort_benchmark_x, effort_benchmark_y)
  
  
  def compute_effort_tmax_benchmark_curve(self, zone, temperature, deb=2020, fin=2100, step=10):
    """Compute the benchmark curve of the effort of an area over tmax.
    
    The benchmark curve over tmax is a curve representing the maximal decarbonisation rate for 
    each tmax (between deb and fin with a step of step)
    
    The benchmark is computed with all the temperature that available in our data set of budget.
    
    The output is a tuple of two list :
      - the first list is the list of the x values : the tmax
      - the second list is the list of the y values : the maximal dacrbonisation rate of the corresponding tmax.
    """
    effort_benchmark_x = range(deb, fin + 1, step)
    effort_benchmark_y = []
    
    for tmax in effort_benchmark_x:
      self.configure_model(temperature, tmax, self.r, self.gamma, self.theta, self.free_sigma)
      (emission_x, emission_y) = self.compute_curves(zone, temperature)
      (decarbonization_x, decarbonization_y) = self.compute_decarbonization_curve(zone)
      
      index_tmax = decarbonization_x.index(self.t_max)
      
      effort_benchmark_y.append(decarbonization_y[index_tmax])
    
    return (effort_benchmark_x, effort_benchmark_y)
      

################

if __name__ == '__main__':
  
  temp = 2.0
  
  dataset = DataSet('EDGAR')
  dataset.load_from_sqlite('climate_data.db')
  mon_model = Model(dataset)
  mon_model.configure_model(temp, 2030, 1.0, 1.0, 1000.0, 1.0)
  
#  country_list = mon_model.dataset.get_country_list()
  
#  tmp = []
  
  (x, y) = mon_model.compute_curves(112, 2.0)
  plt.plot(x, y)
  (x, y) = mon_model.compute_curves(56, 2.0)
  plt.plot(x, y)

  plt.show()
  












































