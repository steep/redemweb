"""
WSGI config for redemWeb project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "redemWeb.settings")
os.environ.setdefault("REDEM_DB_PATH", "/var/www/html/redem/redemweb")

application = get_wsgi_application()
