$("#visualize_button").click(function() {
  var cur_loc = $(location).attr('href');
  
  var languages = cur_loc.substring(cur_loc.lastIndexOf("/"), cur_loc.length);
  
  if(languages == "/") {
    languages = languages + 'English';
  }
  
  var new_location = cur_loc.substring(0, cur_loc.lastIndexOf("/")) + "/redemCurve" + languages;
  $(location).attr('href', new_location);
});