//// Initialisations and global variable

var is_active = "OVERVIEW";
var is_not_computing = true;
var chart_saved = {
  "OVERVIEW" : [],
  "VISUALIZE" : [],
  "EFFORT_VISUALIZATION" : [],
  "EXPERT" : [],
  "ENVELOPE" : [],
  "RANGE" : [],
  "CHALLENGE_YOUR_INDC" : []
};

var color_tab = ["#E67E30", "#370028", "#568203", "#E8D630", "#2C75FF", "#6D071A", "#462E01", "#7FFF00",
                 "#C72C48", "#606060", "#16B84E", "#FFD700", "#000000", "#F88E55", "#3A9D23", "#723E64",
                 "#E97451", "#BEF574", "#D58490", "#FF7F00", "#B3B191", "#B666D2", "#94812B", "#357AB7"];

$(function() {
  $(".panel-collapse").each(function(i){
    $(this).collapse('hide');
  });
  
  $("[data-toggle='tooltip']").tooltip('show');
  
  $(".panel-collapse").first().collapse("show");
  
  $("input[type='radio']").first().prop('checked', true);
  $("input[type='checkbox']").first().prop('checked', true);
  
  $("#indc_checkbox").prop('checked', true);
  
  $("#OVERVIEW_form_chart").fadeIn('Quick');
  
  send_curve_request()
});

////

function get_color_from_tab(ind) {
  ind = ind % color_tab.length;
  return color_tab[ind];
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function make_dark_color(color) {
  var dark_const = 35;
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
  red = Math.max(parseInt(result[1], 16) - dark_const,0);
  green = Math.max(parseInt(result[2], 16) - dark_const,0);
  blue = Math.max(parseInt(result[3], 16) - dark_const,0);
  
  return "#" + componentToHex(red) + componentToHex(green) + componentToHex(blue);
}

function make_post_data(data) {
  out = ""  
  for(key in data) {
    console.log('KEY'+key+'*')
    out += key + "=" + data[key] + "&";
  }
  
  return out.substring(0, out.length - 1);
}

function move_top() {
  var offset = $("#top_of_page").offset().top;
  $('html, body').animate({scrollTop: offset}, 'slow');
}

function change_form(id_form, id_button, box) {
  $("[data-toggle='tooltip']").tooltip('show');
  
  $("footer").fadeOut("quick");
  $("#form_box").fadeOut("quick", function() {
    
    $(".country_box").each(function(i) {
      $(this).prop('class', "country_box " + box);
    });
    $(".region_checkbox").each(function(i) {
      $(this).prop('type', box)
    });
    $(".country_checkbox").each(function(i) {
      $(this).prop('type', box)
    });
  
    $(".container_button_menu").each(function(i) {
      $(this).prop('class', "container_button_menu");
    });
    
    $("#" + id_button).prop('class', "container_button_menu active");
    
  
    $(".redem_form").each(function() {
      $(this).fadeOut("quick");
    });
  
    $("#" + id_form).delay("slow").fadeIn('Quick');
    $("#" + id_form + "_chart").delay("slow").fadeIn('Quick');
  
    $("#form_box").delay("slow").fadeIn("quick");
    $("footer").delay("slow").fadeIn("quick");
  });
}

$("#button_REDEM_WEB").click(function(){ 
  console.log(is_active);
  $("#modal_" + is_active).modal();
  return false;
});

$("#button_overview").click(function() { 

  if(is_not_computing) {
    $("[data-toggle='tooltip']").tooltip('show');
    
    $("footer").fadeOut("quick");
    is_active = "OVERVIEW";
    
    
    $(".container_button_menu").each(function(i) {
      $(this).prop('class', "container_button_menu");
    });
    

    $("#container_button_overview").prop('class', "container_button_menu active");
    
    $("#form_box").fadeOut("quick");
    $(".redem_form").each(function() {
      $(this).fadeOut("quick");
    });
    $("#OVERVIEW_form_chart").delay("slow").fadeIn('Quick');
    $("footer").delay("slow").fadeIn("quick");
    
    move_top(); 
    
  }
  return false;
});

$("#button_visualize").click(function() {
  if(is_not_computing) {
    is_active = "VISUALIZE";
    change_form("VISUALIZE_form", "container_button_visualize", "checkbox");
  
    move_top();
  }
  return false;
});

$("#button_effort_visualization").click(function() {
  if(is_not_computing) {
    is_active = "EFFORT_VISUALIZATION";
    change_form("EFFORT_VISUALIZATION_form", "container_button_effort_visualization", "radio");
  
    move_top();
  }
  return false;
});

/*
$(document).click(function(event) {
  var text = $(event.target).text(); 
  console.log($(event.target));
  console.log(text);
});
*/

$("#button_expert.dropdown-item").click(function() {
  if(is_not_computing) {
    is_active = "EXPERT";
    change_form("EXPERT_form", "container_button_expert_version", "checkbox");
  
    move_top();
  }
  return false;
});

$("#button_envelope.dropdown-item").click(function() {
  if(is_not_computing) {
    is_active = "ENVELOPE";
    change_form("ENVELOPE_form", "container_button_envelope_version", "radio");
  
    move_top();
  }
  return false;
});

$("#button_range.dropdown-item").click(function() { 
  if(is_not_computing) {
    is_active = "RANGE";
    change_form("RANGE_form", "container_button_range_version", "radio");
  
    move_top();
  }
  return false;
});

$("#button_challenge_indc").click(function() {
  if(is_not_computing) {
    is_active = "CHALLENGE_YOUR_INDC";
    change_form("CHALLENGE_YOUR_INDC_form", "container_button_challenge_indc", "radio");
  
    move_top();
  }
  return false;
});

$("#button_author").click(function(){
  $("#modal_author").modal();
  return false;
});

$("#button_credit").click(function(){
  $("#modal_credit").modal();
  return false;
});

$("#button_download").click(function(){
  $("#modal_download").modal();
  return false;
});

$("#button_contact").click(function(){
  $("#modal_contact").modal();
  return false;
});

$("#id_tmax_range").click(function() {
  if($(this).prop('checked')) {
    $("#id_min_range").val("2020");
    $("#id_max_range").val("2100");
    $("#option_range").html('<div class="input-group"><span class="input-group-addon">r</span><input id="id_r_option_range" name="tmax" type="text" class="form-control" value="1"><span class="input-group-addon">&gamma;</span><input id="id_gamma_option_range" name="tmax" type="text" class="form-control" value="2"><span class="input-group-addon">&theta;</span><input id="id_theta_option_range" name="tmax" type="text" class="form-control" value="1000"><span class="input-group-addon">&sigma;</span><input id="id_free_sigma_option_range" name="tmax" type="text" class="form-control" value="3"></div><br>')
  }
});

$("#id_r_range").click(function() {
  if($(this).prop('checked')) {
    $("#id_min_range").val("1");
    $("#id_max_range").val("50");
    $("#option_range").html('<div class="input-group"><span class="input-group-addon">T<SUB>max</SUB></span><input id="id_tmax_option_range" name="tmax" type="text" class="form-control" value="2030"><span class="input-group-addon">&gamma;</span><input id="id_gamma_option_range" name="tmax" type="text" class="form-control" value="2"><span class="input-group-addon">&theta;</span><input id="id_theta_option_range" name="tmax" type="text" class="form-control" value="1000"><span class="input-group-addon">&sigma;</span><input id="id_free_sigma_option_range" name="tmax" type="text" class="form-control" value="3"></div><br>')
  }
});

$("#id_gamma_range").click(function() {
  if($(this).prop('checked')) {
    $("#id_min_range").val("1");
    $("#id_max_range").val("50");
    $("#option_range").html('<div class="input-group"><span class="input-group-addon">T<SUB>max</SUB></span><input id="id_tmax_option_range" name="tmax" type="text" class="form-control" value="2030"><span class="input-group-addon">r</span><input id="id_r_option_range" name="tmax" type="text" class="form-control" value="1"><span class="input-group-addon">&theta;</span><input id="id_theta_option_range" name="tmax" type="text" class="form-control" value="1000"><span class="input-group-addon">&sigma;</span><input id="id_free_sigma_option_range" name="tmax" type="text" class="form-control" value="3"></div><br>')
  }
});

$("#id_theta_range").click(function() {
  if($(this).prop('checked')) {
    $("#id_min_range").val("1");
    $("#id_max_range").val("50");
    $("#option_range").html('<div class="input-group"><span class="input-group-addon">T<SUB>max</SUB></span><input id="id_tmax_option_range" name="tmax" type="text" class="form-control" value="2030"><span class="input-group-addon">r</span><input id="id_r_option_range" name="tmax" type="text" class="form-control" value="1"><span class="input-group-addon">&gamma;</span><input id="id_gamma_option_range" name="tmax" type="text" class="form-control" value="2"><span class="input-group-addon">&sigma;</span><input id="id_free_sigma_option_range" name="tmax" type="text" class="form-control" value="3"></div><br>')
  }
});

$("#id_free_sigma_range").click(function() {
  if($(this).prop('checked')) {
    $("#id_min_range").val("1");
    $("#id_max_range").val("50");
    $("#option_range").html('<div class="input-group"><span class="input-group-addon">T<SUB>max</SUB></span><input id="id_tmax_option_range" name="tmax" type="text" class="form-control" value="2030"><span class="input-group-addon">r</span><input id="id_r_option_range" name="tmax" type="text" class="form-control" value="1"><span class="input-group-addon">&gamma;</span><input id="id_gamma_option_range" name="tmax" type="text" class="form-control" value="2"><span class="input-group-addon">&theta;</span><input id="id_theta_option_range" name="tmax" type="text" class="form-control" value="1000"></div><br>')
  }
});

function get_zone_radio(data) {
  data['zone'] = $('input[name=curve_choice]:checked').prop("id");
  
  return data;
}

function get_zone_checkbox(data) {
  $(".region_checkbox").each(function(i) {
    data[$(this).prop('id')] = $(this).prop('checked');
  });
  $(".country_checkbox").each(function(i) {
    data[$(this).prop('id')] = $(this).prop('checked');
  });
  return data;
}

function get_basic_form_data(data) {
  data['APP'] = is_active;
  data['csrfmiddlewaretoken'] = $("input[name='csrfmiddlewaretoken']").val();
  data['show_indc'] = $('#indc_checkbox').prop('checked');
  data['temperature'] = $('input[name=temperature_choice]:checked').val();
  
  return data; 
}

function get_OVERVIEW_form_data(data) {
  return data;
}

function get_VISUALIZE_form_data(data) {  
  data["tmax"] = $("#id_tmax_visualize").val();
  
  return get_zone_checkbox(data);
}

function get_EFFORT_VISUALIZATION_form_data(data) {
  data["tmax"] = $("#id_tmax_effort_visualization").val();
  
  return get_zone_radio(data);
}

function get_EXPERT_form_data(data) {
  data['tmax'] = $("#id_tmax_expert").val();
  data['r'] = $("#id_r_expert").val();
  data['gamma'] = $("#id_gamma_expert").val();
  data['theta'] = $("#id_theta_expert").val();
  data['free_sigma'] = $("#id_free_sigma_expert").val();
  
  return get_zone_checkbox(data);
}

function get_ENVELOPPE_form_data(data) {
  data['tmax_min'] = $("#tmax_min_envelope").val();
  data['tmax_max'] = $("#tmax_max_envelope").val();
  data['r_min'] = $("#r_min_envelope").val();
  data['r_max'] = $("#r_max_envelope").val();
  data['gamma_min'] = $("#gamma_min_envelope").val();
  data['gamma_max'] = $("#gamma_max_envelope").val();
  data['theta_min'] = $("#theta_min_envelope").val();
  data['theta_max'] = $("#theta_max_envelope").val();
  data['free_sigma_min'] = $("#free_sigma_min_envelope").val();
  data['free_sigma_max'] = $("#free_sigma_max_envelope").val();
  
  return get_zone_radio(data);
}

function get_RANGE_form_data(data) {
  data['param'] = $('input[name=param_choice]:checked').val();
  data['min_v'] = $("#id_min_range").val();
  data['max_v'] = $("#id_max_range").val();
  data['step_v'] = $("#id_step_range").val();
  
  var option_tmax = $("#id_tmax_option_range").val();
  if(typeof option_tmax === "undefined") {option_tmax = 2050;}
  var option_r = $("#id_r_option_range").val();
  if(typeof option_r === "undefined") {option_r = 2;}
  var option_gamma = $("#id_gamma_option_range").val();
  if(typeof option_gamma === "undefined") {option_gamma = 2;}
  var option_theta = $("#id_theta_option_range").val();
  if(typeof option_theta === "undefined") {option_theta = 2;}
  var option_free_sigma = $("#id_free_sigma_option_range").val();
  if(typeof option_free_sigma === "undefined") {option_free_sigma = 2;}
  
  data['option_tmax'] = option_tmax;
  data['option_r'] = option_r;
  data['option_gamma'] = option_gamma;
  data['option_theta'] = option_theta;
  data['option_free_sigma'] = option_free_sigma;
  
  return get_zone_radio(data);
}

function get_CHALLENGE_YOUR_INDC_form_data(data) {
  data['tmax'] = $("#id_tmax_challenge_your_indc").val();
  
  return get_zone_radio(data);
}

function get_data_to_send() {
  var data = {};
  
  var data = get_basic_form_data(data);
  
  switch(is_active) {
    case "OVERVIEW":
      data = get_OVERVIEW_form_data(data);
      break;
    case "VISUALIZE":
      data = get_VISUALIZE_form_data(data);
      break;
    case "EFFORT_VISUALIZATION":
      data = get_EFFORT_VISUALIZATION_form_data(data);
      break;
    case "EXPERT":
      data = get_EXPERT_form_data(data);
      break;
    case "ENVELOPE":
      data = get_ENVELOPPE_form_data(data);
      break;
    case "RANGE":
      data = get_RANGE_form_data(data);
      break;
    case "CHALLENGE_YOUR_INDC":
      data = get_CHALLENGE_YOUR_INDC_form_data(data);
      break;
    default:
      console.log("ERREUR");
  }
  
  return data;
}

function get_series_from_json_default(chart_tab) {
  var series = new Array();
  var ind_color = 0;
  
  
  $.each(chart_tab, function(label, curve) {
    if(label.endsWith("INDC")) {
      series.push({
        name: label,
        data: curve,
        color: get_color_from_tab(ind_color),
        type: "line"
      });
    }
    else {
      series.push({
        name: label,
        data: curve,
        color: get_color_from_tab(ind_color),
      });
      ind_color = ind_color + 1;
    }
  });
  
  
  return series;
}

function get_series_from_json_VISUALIZE(chart_tab) {
  var series = new Array();
  var ind_color = 0;
  
  $.each(chart_tab, function(label, curve_tab) {
    optimistic_curve = curve_tab[0][0];
    optimistic_label = curve_tab[0][1];
    pessimistic_curve = curve_tab[1][0];
    pessimistic_label = curve_tab[1][1];
    
    new_color = get_color_from_tab(ind_color);
    
    series.push({
      name: optimistic_label,
      data: optimistic_curve,
      color: new_color,
    });
    series.push({
      name: pessimistic_label,
      data: pessimistic_curve,
      color: make_dark_color(new_color),
    });
    
    ind_color = ind_color + 1;
  });
  
  return series;
}

function get_series_from_json(chart_tab, chart_nb) {
  switch(is_active) {
    case "VISUALIZE":
      if(chart_nb == 3 || chart_nb == 4) {
        return get_series_from_json_VISUALIZE(chart_tab);
      }
    default:
      return get_series_from_json_default(chart_tab);
  }
}

function get_color(serie) {
  console.log(serie);
  return serie.color;
}

function set_color(serie, new_color) {
  serie.color = new_color;
}

function get_country_of_INDC_color(series_list, serie) {
  var name_serie = serie.name.substring(0, serie.name.length - " INDC".length);
  console.log("name_serie : " + name_serie);
  var i;
  for(i = 0 ; i < series_list.length ; i++) {
    console.log(series_list[i].name + " ?= " + name_serie);
    if(series_list[i].name == name_serie) {
      console.log("OK : ");
      console.log(series_list[i]);
      return get_color(series_list[i]);
    }
  }
}

function make_basic_chart_option(new_title, x_title, y_title, series) {
  return {
    chart: {
      type: 'line',
      panning: true,
      pinchType: 'x',
      inverted: false
    },
    title: {text: new_title},
    xAxis: {
      title: {text: x_title},
      reversed: false
    },
    yAxis: {title: {text: y_title}},
    series: series,
    exporting: {
      csv: {
        itemDelimiter: ";"
      },
    },
    navigator: {
      enabled: true,
      series: {
        includeInCSVExport: false
      },
      xAxis: {
        dateTimeLabelFormats: {
          millisecond: '',
          second: '',
          minute: '',
          hour: '',
          day: '',
          week: '',
          month: '',
          year: ''
        }
      }
    },
  }
}

function find_conditional_serie(series, id_cond_serie) {
  var name_serie = [id_cond_serie + ' conditional'].join();
  console.log(name_serie);
  var out = {
    index : -1,
    data : []
  };
  $.each(series, function(i, serie) {
    if(serie.name == name_serie) {
      out.data = serie.data;
      out.index = i;
    }
  });
  
  return out;
}

function permute_series(series, permutation_tab) {
  var tmp;
  $.each(permutation_tab, function(i, permutation) {
    tmp = series[permutation[0]];
    series[permutation[0]] = series[permutation[1]];
    series[permutation[1]] = tmp;
  });
}

function make_stack_gap_indc(series) {
  var permutation = [];
  
  $.each(series, function(i, serie) {
    var tmp = serie.name.split(' ');
    var is_unconditional = tmp.pop();
    var stack_id = tmp.join(' ');
    if(is_unconditional == 'unconditional') {
      var serie_cond = find_conditional_serie(series, stack_id);
      console.log(serie_cond);
      if(serie_cond.data.length != 0) {
        $.each(serie.data, function(i, data) {
          data[1] = data[1] - serie_cond.data[i][1];
        });
        permutation.push([i, serie_cond.index]);
      }
    }
    serie['stack'] = stack_id;
  });
  
  permute_series(series, permutation);
}

function custom_chart_option_OVERVIEW(option) {
  option[2].navigator.enabled = false;
  option[3].navigator.enabled = false;
  
  return option;
}

function custom_chart_option_VISUALIZE(option) {
  option[2].chart.type = "column";
  option[3].chart.type = "column";
  
  option[2].navigator.enabled = false;
  option[3].navigator.enabled = false;
  
//  make_stack_gap_indc(option[2].series);
  
//  make_stack_gap_indc(option[3].series);
  
  return option;
}

function custom_chart_option_EFFORT_VISUALIZATION(option) {
  option[0].chart.type = "column";
  option[1].chart.type = "column";
  
  option[0].xAxis.reversed = true;
  option[1].xAxis.reversed = true;
  
  option[0].navigator.enabled = false;
  option[1].navigator.enabled = false;
  option[2].navigator.enabled = false;
  option[3].navigator.enabled = false;
  
  console.log("ALLEZ HOP, CA DEGAGE !!!!!")
  
  return option;
}

function custom_chart_option_CHALLENGE_YOUR_INDC(option) {
  option[2].chart.type = "column";
  option[3].chart.type = "column";
  
  option[2].navigator.enabled = false;
  option[3].navigator.enabled = false;
  
  return option;
}

function custom_chart_option_ENVELOPE(option) {
  option[0].chart.type = "arearange";
  option[1].chart.type = "arearange";
  
  return option;
}

function bold_INDC(option, width) {
  $.each(option, function(i, my_option) {
    $.each(my_option.series, function(i, serie) {
      if(serie.name.endsWith("INDC")) {
        serie["lineWidth"] = width;
        // Pour RANGE, les courbes ont toutes des couleurs différentes
        if(is_active != "RANGE" && is_active != "OVERVIEW") {
          var zone = serie.name.substring(0, serie.name.length - " INDC".length);
          var serie_assos = my_option.series.find(function(s) {return s.name == zone;});
          serie.color = serie_assos.color;
          console.log("KKKKKKKKKKKKKKKIIJIJIJOJIOJOJ");
          console.log(serie_assos);
        }
      };
    });
  });
}

function custom_chart_option_with_request(option) {
  bold_INDC(option, 10);
  console.log(JSON.stringify(option));
  switch(is_active) {
    case "OVERVIEW":
      return custom_chart_option_OVERVIEW(option);
    case "VISUALIZE":
      return custom_chart_option_VISUALIZE(option);
    case "EFFORT_VISUALIZATION":
      return custom_chart_option_EFFORT_VISUALIZATION(option);
    case "CHALLENGE_YOUR_INDC":
      return custom_chart_option_CHALLENGE_YOUR_INDC(option);
    case "ENVELOPE":
      return custom_chart_option_ENVELOPE(option);
    default:
      return option;
  }
}

function draw_curve_on_chart(id_chart, chart_option) {
  if(chart_option.series.length != 0) {
    $("#" + is_active + "_container_" + id_chart).prop('hidden', false);
    console.log("#" + is_active + "_" + id_chart);
    return $("#" + is_active + "_" + id_chart).highcharts(chart_option);
  }
  else {
    $("#" + is_active + "_container_" + id_chart).prop('hidden', true);
  }
}

function CHALLENGE_YOUR_INDC_other_treatment(json) {
  $("#jumbotron_CHALLENGE_YOUR_INDC_1").html(json.JUMBOTRON_CONTENT);
}

function other_request_treatment(json) {
  switch(is_active) {
    case "CHALLENGE_YOUR_INDC":
      CHALLENGE_YOUR_INDC_other_treatment(json);
      break;
    default:
      break;
  }
}

function send_curve_request() {

  is_not_computing = false;
  var data_post = get_data_to_send();

  var plot_html = $("#plot_button").html();
  
  $("#plot_button").html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Computing')
  
  $.ajax({
    url : '',
    type : 'POST',
    data : make_post_data(data_post),
    dataType : 'json',
    error : function(resultat, statut, erreur) {
      $("#plot_button").html(plot_html)
    },
    success : function(json, statut) {
      //var json_obj = jQuery.parseJSON(json);
      $("#plot_button").html(plot_html)
      
      console.log(json);
      
      var charts1_series = get_series_from_json(json.CHARTS1, 1);
      var charts2_series = get_series_from_json(json.CHARTS2, 2);
      var charts3_series = get_series_from_json(json.CHARTS3, 3);
      var charts4_series = get_series_from_json(json.CHARTS4, 4);
      
      
      console.log("CHART SERIE ICI");
      console.log(charts1_series);
      
      var charts_option = [
        make_basic_chart_option(json.CHARTS1_TITLE, json.CHARTS1_X_LABEL, json.CHARTS1_Y_LABEL, charts1_series),
        make_basic_chart_option(json.CHARTS2_TITLE, json.CHARTS2_X_LABEL, json.CHARTS2_Y_LABEL, charts2_series),
        make_basic_chart_option(json.CHARTS3_TITLE, json.CHARTS3_X_LABEL, json.CHARTS3_Y_LABEL, charts3_series),
        make_basic_chart_option(json.CHARTS4_TITLE, json.CHARTS4_X_LABEL, json.CHARTS4_Y_LABEL, charts4_series),
      ];
      
      charts_option = custom_chart_option_with_request(charts_option);
      
      chart_saved[is_active] = charts_option;
      
      console.log(is_active);
      
      var is_draw_1 = draw_curve_on_chart("charts_number_1", charts_option[0]);
      var is_draw_2 = draw_curve_on_chart("charts_number_2", charts_option[1]);
      var is_draw_3 = draw_curve_on_chart("charts_number_3", charts_option[2]);
      var is_draw_4 = draw_curve_on_chart("charts_number_4", charts_option[3]);
      
      if (typeof is_draw_1 != 'undefined') {
        $("#jumbotron_" + is_active + "_1").prop("hidden", false);
      }
      
      if (typeof is_draw_3 != 'undefined') {
        $("#jumbotron_" + is_active + "_2").prop("hidden", false);
      }
      
      console.log("is_draw_1");
      console.log(is_draw_1);
      console.log("is_draw_2");
      console.log(is_draw_2);
      console.log("is_draw_3");
      console.log(is_draw_3);
      console.log("is_draw_4");
      console.log(is_draw_4);
      
      other_request_treatment(json);
      
      console.log("OK");
      move_top();
      console.log("Bouger");
      
      $(window).resize();
      
      console.log(JSON.stringify(chart_saved));
      is_not_computing = true;
    },
  });
}


$("#plot_button").click(function() {
  send_curve_request();
});


$(".button_languages").click(function() {
  var languages = $(this).html();
  var cur_loc = $(location).attr('href');
  var new_location = cur_loc.substring(0, cur_loc.lastIndexOf("/")) + "/" + languages;
  $(location).attr('href', new_location);
});







































